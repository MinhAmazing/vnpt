<?php
session_start();
$conn=mysqli_connect("localhost","root","07121998","qlbansach");
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}
mysqli_query($conn,"SET NAMES 'utf8'");
if(isset($_GET['id']))
{
    foreach ($_SESSION['cart'] as $key =>$item){
        if($item["MaSach"]==$_GET['id']){
            $_SESSION['cart'][$key]['soLuong']=$_GET['soLuong'];
            if($_SESSION['cart'][$key]['soLuong']==0)
                unset($_SESSION['cart'][$key]);
            break;
        }
    }
}
echo '<div class="span8">
                                                        <div class="box-list-cart">
                                                            <div class="list-sp">
                                                                <div>
                                                                    <div class="mainbox-cart-body">
                                                                        <div id="cart_items">
                                                                            <div class="table-cart-items">
                                                                                <table>
                                                                                    <thead>
                                                                                    <tr>
                                                                                        <th colspan="3">SẢN PHẨM
                                                                                        </th>
                                                                                    </tr>
                                                                                    </thead>
                                                                                    <tbody class="updown_wrapper">
                                                                                    <form>';

                                                                                        foreach ($_SESSION['cart'] as $key => $item) {
                                                                                        echo '
                                                                                            <tr>
                                                                                                <td width="8%">
                                                                                                    <div class="thumb-sp">
                                                                                                        <img src="'.$item['Anh'].'"
                                                                                                             alt="">
                                                                                                    </div>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <p
                                                                                                            class="tilte-item-sp">
                                                                                                        '.$item['TenSach'].'</p>
                                                                                                    <div class="input-group"
                                                                                                         data-max-amount="2">
                                                                                                        <button
                                                                                                                type="button"
                                                                                                                class="btn btn-default btnTru" onclick="tru(\''.$item['MaSach'].'\')">-</button>
                                                                                                        <input value="'.$item['soLuong'].'"
                                                                                                               type="number"
                                                                                                               name=""
                                                                                                               class="change"
                                                                                                               readonly="">
                                                                                                        <button
                                                                                                                type="button"
                                                                                                                class="btn btn-default btnCong" onclick="cong(\''.$item['MaSach'].'\')">+</button>
                                                                                                    </div>
                                                                                                </td>
                                                                                                <td width="20%"
                                                                                                    align="right">
                                                                                                    <p class="lable-preice"
                                                                                                       data-price="<?=$item[\'Gia\']?>">
                                                                                                    <span
                                                                                                            class="checkout-item-value">'.$item['soLuong'].'</span>
                                                                                                        x <span
                                                                                                                id="sec_price_2391585124"
                                                                                                                class="none">'.$item['Gia'].'</span>&nbsp;<span
                                                                                                                class="none">₫</span>
                                                                                                    </p> <a
                                                                                                            class="cm-ajax  cm-ajax-full-render clear remove-product" onclick="change(\''.$item['MaSach'].'\',0)"
                                                                                                            title="Xoá"><i
                                                                                                                class="recycle-o fa fa-trash-o"
                                                                                                                aria-hidden="true"></i></a>
                                                                                                </td>
                                                                                            </tr>';
                                                                                        }
                                                                                    echo '</form>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                            <!--cart_items-->
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="clear"></div>
                                                            </div>
                                                        </div>
                                                    </div>';
echo '<div class="span4">
                                                        <div class="box-tom-tat-don-hang">
                                                            <div class="title-tom-tat">
                                                                TÓM TẮT ĐƠN HÀNG
                                                            </div>
                                                            <div class="list-tom-tat-thanh-toan">
                                                                <table>
                                                                    <tbody>
                                                                    <tr>';
                                                                        $soluong=0;
                                                                                    $tong=0;

                                                                                    foreach ($_SESSION['cart'] as $key =>$value){
                                                                                        $soluong+=$value['soLuong'];
                                                                                        $tong+=$value['Gia']*$value['soLuong'];
                                                                                    }
                                                                               echo '     
                                                                        <td width="60%">Sản phẩm</td>
                                                                        <td width="40%" align="right"> <span
                                                                                    class="checkout-amount-value">'.$soluong.'
                                                                                </span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="60%">Phí vận chuyển</td>
                                                                        <td width="40%" align="right">
                                                                            Miễn phí
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="60%" class="bold">TẠM TÍNH</td>
                                                                        <td class="color-green" width="40%"
                                                                            align="right"><span
                                                                                    class="checkout-item-value"><span
                                                                                        id="sec_subtotal_cart"
                                                                                        class="none">'.$tong.'</span>&nbsp;<span
                                                                                        class="none">₫</span></span>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                                <div class="box-tong-cong">
                                                                    <div class="not-label bold">Tổng cộng</div>
                                                                    <div class="not-number"><span
                                                                                class="checkout-item-value"><span
                                                                                    id="sec_total_cart"
                                                                                    class="none">'.$tong.'</span>&nbsp;<span
                                                                                    class="none">₫</span></span></div>
                                                                </div>
                                                                <div class="hidden"> <span id="wrap_button_cart"
                                                                                           class="button-submit button-wrap-left"><span
                                                                                class="button-submit button-wrap-right"><input
                                                                                    id="button_cart" class="button_cart"
                                                                                    type="submit"
                                                                                    name="dispatch[checkout.update]"
                                                                                    value="Cập nhập giỏ hàng"></span></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>';