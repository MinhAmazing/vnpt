
// Copyright 2012 Google Inc. All rights reserved.
(function(w,g){w[g]=w[g]||{};w[g].e=function(s){return eval(s);};})(window,'google_tag_manager');(function(){

var data = {
"resource": {
  "version":"141",
  
  "macros":[{
      "function":"__v",
      "vtp_name":"gtm.element",
      "vtp_dataLayerVersion":1
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){var a=",["escape",["macro",0],8,16],".querySelector(\".header-search-input\");return a?a.value:void 0})();"]
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){var a=navigator.userAgent||navigator.vendor||window.opera;return\/(android|bb\\d+|meego).+mobile|avantgo|bada\\\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\\\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\\.(browser|link)|vodafone|wap|windows ce|xda|xiino\/i.test(a)||\/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\\-(n|u)|c55\\\/|capi|ccwa|cdm\\-|cell|chtm|cldc|cmd\\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\\-s|devi|dica|dmob|do(c|p)o|ds(12|\\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\\-|_)|g1 u|g560|gene|gf\\-5|g\\-mo|go(\\.w|od)|gr(ad|un)|haie|hcit|hd\\-(m|p|t)|hei\\-|hi(pt|ta)|hp( i|ip)|hs\\-c|ht(c(\\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\\-(20|go|ma)|i230|iac( |\\-|\\\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\\\/)|klon|kpt |kwc\\-|kyo(c|k)|le(no|xi)|lg( g|\\\/(k|l|u)|50|54|\\-[a-w])|libw|lynx|m1\\-w|m3ga|m50\\\/|ma(te|ui|xo)|mc(01|21|ca)|m\\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\\-2|po(ck|rt|se)|prox|psio|pt\\-g|qa\\-a|qc(07|12|21|32|60|\\-[2-7]|i\\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\\\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\\-|oo|p\\-)|sdk\\\/|se(c(\\-|0|1)|47|mc|nd|ri)|sgh\\-|shar|sie(\\-|m)|sk\\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\\-|v\\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\\-|tdg\\-|tel(i|m)|tim\\-|t\\-mo|to(pl|sh)|ts(70|m\\-|m3|m5)|tx\\-9|up(\\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\\-|your|zeto|zte\\-\/i.test(a.substr(0,\n4))})();"]
    },{
      "function":"__v",
      "vtp_name":"gtm.element",
      "vtp_dataLayerVersion":1
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){var a=",["escape",["macro",3],8,16],";consoel.log(a)})();"]
    },{
      "function":"__e"
    },{
      "function":"__gas",
      "vtp_cookieDomain":"auto",
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_useHashAutoLink":false,
      "vtp_decorateFormsAutoLink":false,
      "vtp_enableLinkId":false,
      "vtp_enableEcommerce":false,
      "vtp_trackingId":"UA-2029810-2",
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false
    },{
      "function":"__u",
      "vtp_enableMultiQueryKeys":false,
      "vtp_enableIgnoreEmptyQueryParam":false
    },{
      "function":"__u",
      "vtp_component":"URL",
      "vtp_enableMultiQueryKeys":false,
      "vtp_enableIgnoreEmptyQueryParam":false
    },{
      "function":"__v",
      "vtp_name":"gtm.elementId",
      "vtp_dataLayerVersion":1
    },{
      "function":"__v",
      "vtp_name":"gtm.triggers",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":true,
      "vtp_defaultValue":""
    },{
      "function":"__aev",
      "vtp_setDefaultValue":false,
      "vtp_varType":"ATTRIBUTE",
      "vtp_attribute":"title"
    },{
      "function":"__v",
      "vtp_name":"gtm.videoStatus",
      "vtp_dataLayerVersion":1
    },{
      "function":"__v",
      "vtp_name":"gtm.videoTitle",
      "vtp_dataLayerVersion":1
    },{
      "function":"__aev",
      "vtp_varType":"TEXT"
    },{
      "function":"__v",
      "vtp_name":"gtm.elementClasses",
      "vtp_dataLayerVersion":1
    },{
      "function":"__v",
      "vtp_name":"gtm.elementUrl",
      "vtp_dataLayerVersion":1
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"PageClass"
    },{
      "function":"__aev",
      "vtp_setDefaultValue":false,
      "vtp_varType":"ATTRIBUTE",
      "vtp_attribute":"name"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"ProductDetail.productID"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"ProductDetail.productName"
    },{
      "function":"__v",
      "vtp_name":"gtm.videoPercent",
      "vtp_dataLayerVersion":1
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"UserID"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"UserFirstName"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"UserEmail"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"UserLocation"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"AccesstradeUrl"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"CatID"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"LastPurchasedDate"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"PurchasedCateIds"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"OrdersProducts"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"CartProducts"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"Products"
    },{
      "function":"__u",
      "vtp_component":"PATH",
      "vtp_enableMultiQueryKeys":false,
      "vtp_enableIgnoreEmptyQueryParam":false
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"ProductIDs"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"ProductPrice"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"CartSubtotal"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"OrdersDetail"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"UserBirthday"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"UserGender"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"RegisterDate"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"ProductDetail"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"NewsletterEmail"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"FacebookTrackingUrl"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"ChinUrl"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"LastViewCatIDs"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"VarName"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"GoogleAdServicesUrl"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"IpriceUrl"
    },{
      "function":"__u",
      "vtp_component":"HOST",
      "vtp_enableMultiQueryKeys":false,
      "vtp_enableIgnoreEmptyQueryParam":false
    },{
      "function":"__f",
      "vtp_component":"URL"
    },{
      "function":"__e"
    },{
      "function":"__v",
      "vtp_name":"gtm.elementId",
      "vtp_dataLayerVersion":1
    },{
      "function":"__v",
      "vtp_name":"gtm.elementTarget",
      "vtp_dataLayerVersion":1
    },{
      "function":"__v",
      "vtp_name":"gtm.elementClasses",
      "vtp_dataLayerVersion":1
    },{
      "function":"__v",
      "vtp_name":"gtm.elementTarget",
      "vtp_dataLayerVersion":1
    },{
      "function":"__v",
      "vtp_name":"gtm.elementUrl",
      "vtp_dataLayerVersion":1
    },{
      "function":"__aev",
      "vtp_varType":"TEXT"
    },{
      "function":"__cid"
    },{
      "function":"__hid"
    },{
      "function":"__v",
      "vtp_name":"gtm.videoProvider",
      "vtp_dataLayerVersion":1
    },{
      "function":"__v",
      "vtp_name":"gtm.videoUrl",
      "vtp_dataLayerVersion":1
    },{
      "function":"__v",
      "vtp_name":"gtm.videoDuration",
      "vtp_dataLayerVersion":1
    },{
      "function":"__v",
      "vtp_name":"gtm.videoVisible",
      "vtp_dataLayerVersion":1
    },{
      "function":"__v",
      "vtp_name":"gtm.videoCurrentTime",
      "vtp_dataLayerVersion":1
    }],
  "tags":[{
      "function":"__ua",
      "once_per_load":true,
      "vtp_overrideGaSettings":false,
      "vtp_trackType":"TRACK_PAGEVIEW",
      "vtp_gaSettings":["macro",6],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "tag_id":2
    },{
      "function":"__sp",
      "once_per_load":true,
      "vtp_conversionId":"985669925",
      "vtp_customParamsFormat":"NONE",
      "vtp_enableOgtRmktParams":true,
      "vtp_url":["macro",7],
      "tag_id":5
    },{
      "function":"__paused",
      "vtp_originalTagType":"html",
      "tag_id":8
    },{
      "function":"__paused",
      "vtp_originalTagType":"html",
      "tag_id":31
    },{
      "function":"__awct",
      "once_per_load":true,
      "vtp_enableConversionLinker":true,
      "vtp_conversionCookiePrefix":"_gcl",
      "vtp_conversionId":"985669925",
      "vtp_conversionLabel":"xuchCJP7vQkQpcKA1gM",
      "vtp_url":["macro",7],
      "vtp_enableProductReportingCheckbox":false,
      "vtp_enableEnhancedConversionsCheckbox":false,
      "tag_id":32
    },{
      "function":"__gclidw",
      "once_per_event":true,
      "vtp_enableCrossDomain":false,
      "vtp_enableCookieOverrides":false,
      "vtp_enableCrossDomainFeature":true,
      "vtp_enableCookieUpdateFeature":false,
      "tag_id":33
    },{
      "function":"__ua",
      "setup_tags":["list",["tag",0,1]],
      "once_per_load":true,
      "vtp_overrideGaSettings":true,
      "vtp_trackType":"TRACK_TRANSACTION",
      "vtp_gaSettings":["macro",6],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsTransaction":true,
      "tag_id":34
    },{
      "function":"__ua",
      "setup_tags":["list",["tag",0,1]],
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":false,
      "vtp_eventCategory":"search",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_gaSettings":["macro",6],
      "vtp_eventAction":"submit",
      "vtp_eventLabel":["macro",1],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":35
    },{
      "function":"__ua",
      "setup_tags":["list",["tag",0,1]],
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":false,
      "vtp_eventCategory":"home best this week",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_gaSettings":["macro",6],
      "vtp_eventAction":"click",
      "vtp_eventLabel":["macro",11],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":36
    },{
      "function":"__ua",
      "setup_tags":["list",["tag",0,1]],
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":false,
      "vtp_eventCategory":"home block bestseller",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_gaSettings":["macro",6],
      "vtp_eventAction":"click",
      "vtp_eventLabel":["macro",11],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":37
    },{
      "function":"__ua",
      "setup_tags":["list",["tag",0,1]],
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":false,
      "vtp_eventCategory":"home big banner",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_gaSettings":["macro",6],
      "vtp_eventAction":"click",
      "vtp_eventLabel":["macro",11],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":38
    },{
      "function":"__ua",
      "setup_tags":["list",["tag",0,1]],
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":false,
      "vtp_eventCategory":"home small banner 1",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_gaSettings":["macro",6],
      "vtp_eventAction":"click",
      "vtp_eventLabel":["macro",11],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":39
    },{
      "function":"__ua",
      "setup_tags":["list",["tag",0,1]],
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":false,
      "vtp_eventCategory":"home small banner 2",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_gaSettings":["macro",6],
      "vtp_eventAction":"click",
      "vtp_eventLabel":["macro",11],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":40
    },{
      "function":"__ua",
      "setup_tags":["list",["tag",0,1]],
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":false,
      "vtp_eventCategory":"home small banner 3",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_gaSettings":["macro",6],
      "vtp_eventAction":"click",
      "vtp_eventLabel":["macro",11],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":41
    },{
      "function":"__ua",
      "setup_tags":["list",["tag",0,1]],
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":false,
      "vtp_eventCategory":"home small banner 4",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_gaSettings":["macro",6],
      "vtp_eventAction":"click",
      "vtp_eventLabel":["macro",11],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":42
    },{
      "function":"__ua",
      "setup_tags":["list",["tag",0,1]],
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":false,
      "vtp_eventCategory":"home small banner 5",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_gaSettings":["macro",6],
      "vtp_eventAction":"click",
      "vtp_eventLabel":["macro",11],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":43
    },{
      "function":"__ua",
      "setup_tags":["list",["tag",0,0]],
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":false,
      "vtp_eventCategory":"home newspapers introduce",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_gaSettings":["macro",6],
      "vtp_eventAction":"click",
      "vtp_eventLabel":["macro",11],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":44
    },{
      "function":"__ua",
      "setup_tags":["list",["tag",0,1]],
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":false,
      "vtp_eventCategory":"home new imported",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_gaSettings":["macro",6],
      "vtp_eventAction":"click",
      "vtp_eventLabel":["macro",11],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":45
    },{
      "function":"__ua",
      "setup_tags":["list",["tag",0,1]],
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":false,
      "vtp_eventCategory":"home block truyen ke cho be",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_gaSettings":["macro",6],
      "vtp_eventAction":"click",
      "vtp_eventLabel":["macro",11],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":46
    },{
      "function":"__ua",
      "setup_tags":["list",["tag",0,1]],
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":false,
      "vtp_eventCategory":"home block highlight author",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_gaSettings":["macro",6],
      "vtp_eventAction":"click",
      "vtp_eventLabel":["macro",11],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":47
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":false,
      "vtp_eventCategory":"home video youtube",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_gaSettings":["macro",6],
      "vtp_eventAction":["macro",12],
      "vtp_eventLabel":["macro",13],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":48
    },{
      "function":"__ua",
      "setup_tags":["list",["tag",0,1]],
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":false,
      "vtp_eventCategory":"home products highlight author",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_gaSettings":["macro",6],
      "vtp_eventAction":"click",
      "vtp_eventLabel":["macro",11],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":49
    },{
      "function":"__ua",
      "setup_tags":["list",["tag",0,1]],
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":false,
      "vtp_eventCategory":"search suggest",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_gaSettings":["macro",6],
      "vtp_eventAction":"click",
      "vtp_eventLabel":["macro",14],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":50
    },{
      "function":"__ua",
      "setup_tags":["list",["tag",0,1]],
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":false,
      "vtp_eventCategory":"main categories",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_gaSettings":["macro",6],
      "vtp_eventAction":"click",
      "vtp_eventLabel":["macro",11],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":51
    },{
      "function":"__ua",
      "setup_tags":["list",["tag",0,1]],
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":false,
      "vtp_eventCategory":"home button read more",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_gaSettings":["macro",6],
      "vtp_eventAction":"click",
      "vtp_eventLabel":["macro",16],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":52
    },{
      "function":"__ua",
      "setup_tags":["list",["tag",0,1]],
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":false,
      "vtp_eventCategory":"search result",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_gaSettings":["macro",6],
      "vtp_eventAction":"click",
      "vtp_eventLabel":["macro",11],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":53
    },{
      "function":"__ua",
      "setup_tags":["list",["tag",0,1]],
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":false,
      "vtp_eventCategory":"home block recent view",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_gaSettings":["macro",6],
      "vtp_eventAction":"click",
      "vtp_eventLabel":["macro",11],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":54
    },{
      "function":"__ua",
      "setup_tags":["list",["tag",0,1]],
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":false,
      "vtp_eventCategory":"breadcrumbs",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_gaSettings":["macro",6],
      "vtp_eventAction":"click",
      "vtp_eventLabel":["macro",14],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":55
    },{
      "function":"__ua",
      "setup_tags":["list",["tag",0,1]],
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":false,
      "vtp_eventCategory":"page products authors",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_gaSettings":["macro",6],
      "vtp_eventAction":"click",
      "vtp_eventLabel":["macro",11],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":56
    },{
      "function":"__ua",
      "setup_tags":["list",["tag",0,1]],
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":false,
      "vtp_eventCategory":"product detail author",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_gaSettings":["macro",6],
      "vtp_eventAction":"click",
      "vtp_eventLabel":["macro",11],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":57
    },{
      "function":"__ua",
      "setup_tags":["list",["tag",0,0]],
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":false,
      "vtp_eventCategory":"product detail publisher",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_gaSettings":["macro",6],
      "vtp_eventAction":"click",
      "vtp_eventLabel":["macro",11],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":58
    },{
      "function":"__ua",
      "setup_tags":["list",["tag",0,1]],
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":false,
      "vtp_eventCategory":"product catalogue view",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_gaSettings":["macro",6],
      "vtp_eventAction":"click",
      "vtp_eventLabel":["macro",11],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":59
    },{
      "function":"__ua",
      "setup_tags":["list",["tag",0,1]],
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":false,
      "vtp_eventCategory":"button add to cart detail product",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_gaSettings":["macro",6],
      "vtp_eventAction":"click",
      "vtp_eventLabel":["template",["macro",19]," - ",["macro",20]],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":60
    },{
      "function":"__ua",
      "setup_tags":["list",["tag",0,1]],
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":false,
      "vtp_eventCategory":"button add to cart purchase included",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_gaSettings":["macro",6],
      "vtp_eventAction":"click",
      "vtp_eventLabel":["template","Parent: ",["macro",19]," - ",["macro",20]],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":61
    },{
      "function":"__ua",
      "setup_tags":["list",["tag",0,1]],
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":false,
      "vtp_eventCategory":"product recommend",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_gaSettings":["macro",6],
      "vtp_eventAction":"click",
      "vtp_eventLabel":["macro",11],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":62
    },{
      "function":"__ua",
      "setup_tags":["list",["tag",0,1]],
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"show menu catalogue",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_gaSettings":["macro",6],
      "vtp_eventAction":"click",
      "vtp_eventLabel":["macro",17],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":63
    },{
      "function":"__ua",
      "setup_tags":["list",["tag",0,1]],
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":false,
      "vtp_eventCategory":"categories-sidebar special category",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_gaSettings":["macro",6],
      "vtp_eventAction":"click",
      "vtp_eventLabel":["macro",11],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":64
    },{
      "function":"__ua",
      "setup_tags":["list",["tag",0,1]],
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":false,
      "vtp_eventCategory":"categories-sidebar category",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_gaSettings":["macro",6],
      "vtp_eventAction":"click",
      "vtp_eventLabel":["macro",11],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":65
    },{
      "function":"__ua",
      "setup_tags":["list",["tag",0,1]],
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":false,
      "vtp_eventCategory":"categories-sidebar author",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_gaSettings":["macro",6],
      "vtp_eventAction":"click",
      "vtp_eventLabel":["macro",11],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":66
    },{
      "function":"__ua",
      "setup_tags":["list",["tag",0,1]],
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":false,
      "vtp_eventCategory":"categories-sidebar publishers",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_gaSettings":["macro",6],
      "vtp_eventAction":"click",
      "vtp_eventLabel":["macro",11],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":67
    },{
      "function":"__ua",
      "setup_tags":["list",["tag",0,1]],
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":false,
      "vtp_eventCategory":"keywords often searched",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_gaSettings":["macro",6],
      "vtp_eventAction":"click",
      "vtp_eventLabel":["macro",11],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":68
    },{
      "function":"__cl",
      "tag_id":73
    },{
      "function":"__fsl",
      "vtp_waitForTags":false,
      "vtp_checkValidation":false,
      "vtp_waitForTagsTimeout":"2000",
      "vtp_uniqueTriggerId":"10569849_127",
      "tag_id":74
    },{
      "function":"__cl",
      "tag_id":75
    },{
      "function":"__cl",
      "tag_id":76
    },{
      "function":"__cl",
      "tag_id":77
    },{
      "function":"__cl",
      "tag_id":78
    },{
      "function":"__cl",
      "tag_id":79
    },{
      "function":"__cl",
      "tag_id":80
    },{
      "function":"__cl",
      "tag_id":81
    },{
      "function":"__cl",
      "tag_id":82
    },{
      "function":"__cl",
      "tag_id":83
    },{
      "function":"__cl",
      "tag_id":84
    },{
      "function":"__cl",
      "tag_id":85
    },{
      "function":"__ytl",
      "vtp_progressThresholdsPercent":["macro",21],
      "vtp_captureComplete":true,
      "vtp_captureStart":true,
      "vtp_fixMissingApi":true,
      "vtp_triggerStartOption":"WINDOW_LOAD",
      "vtp_radioButtonGroup1":"PERCENTAGE",
      "vtp_capturePause":true,
      "vtp_captureProgress":true,
      "vtp_uniqueTriggerId":"10569849_161",
      "vtp_enableTriggerStartOption":true,
      "tag_id":86
    },{
      "function":"__cl",
      "tag_id":87
    },{
      "function":"__cl",
      "tag_id":88
    },{
      "function":"__cl",
      "tag_id":89
    },{
      "function":"__cl",
      "tag_id":90
    },{
      "function":"__cl",
      "tag_id":91
    },{
      "function":"__cl",
      "tag_id":92
    },{
      "function":"__cl",
      "tag_id":93
    },{
      "function":"__cl",
      "tag_id":94
    },{
      "function":"__cl",
      "tag_id":95
    },{
      "function":"__cl",
      "tag_id":96
    },{
      "function":"__cl",
      "tag_id":97
    },{
      "function":"__cl",
      "tag_id":98
    },{
      "function":"__cl",
      "tag_id":99
    },{
      "function":"__cl",
      "tag_id":100
    },{
      "function":"__cl",
      "tag_id":101
    },{
      "function":"__cl",
      "tag_id":102
    },{
      "function":"__cl",
      "tag_id":103
    },{
      "function":"__cl",
      "tag_id":104
    },{
      "function":"__cl",
      "tag_id":105
    },{
      "function":"__cl",
      "tag_id":106
    },{
      "function":"__cl",
      "tag_id":107
    },{
      "function":"__html",
      "once_per_load":true,
      "vtp_html":"\n\u003Cscript type=\"text\/gtmscript\"\u003Evar pp_gemius_identifier=\"bQ0wqwNx72pIZdPcs7I_zJa176ERCwNk_3x6MohFMrD.X7\";function gemius_pending(a){window[a]=window[a]||function(){var b=window[a+\"_pdata\"]=window[a+\"_pdata\"]||[];b[b.length]=arguments}}gemius_pending(\"gemius_hit\");gemius_pending(\"gemius_event\");gemius_pending(\"pp_gemius_hit\");gemius_pending(\"pp_gemius_event\");\n(function(a,b){try{var c=a.createElement(b),d=a.getElementsByTagName(b)[0],e=\"http\"+(\"https:\"==location.protocol?\"s\":\"\");c.setAttribute(\"async\",\"async\");c.setAttribute(\"defer\",\"defer\");c.src=e+\":\/\/hc.viam.com.vn\/xgemius.js\";d.parentNode.insertBefore(c,d)}catch(f){}})(document,\"script\");\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":1
    },{
      "function":"__html",
      "once_per_load":true,
      "vtp_html":"\u003Cdiv id=\"fb-root\"\u003E\u003C\/div\u003E\n\u003Cscript type=\"text\/gtmscript\"\u003E(function(a,b,c){var d=a.getElementsByTagName(b)[0];a.getElementById(c)||(a=a.createElement(b),a.id=c,a.src=\"\/\/connect.facebook.net\/vi_VN\/sdk.js#xfbml\\x3d1\\x26version\\x3dv3.2\\x26appId\\x3d235384486636984\\x26autoLogAppEvents\\x3d1\",d.parentNode.insertBefore(a,d))})(document,\"script\",\"facebook-jssdk\");\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":3
    },{
      "function":"__html",
      "once_per_load":true,
      "vtp_html":["template","\n    \u003Clink rel=\"manifest\" href=\"\/manifest.json\"\u003E\n\t\u003Cscript data-gtmsrc=\"https:\/\/cdn.onesignal.com\/sdks\/OneSignalSDK.js\" async type=\"text\/gtmscript\"\u003E\u003C\/script\u003E\n\t\u003Cscript type=\"text\/gtmscript\"\u003Ewindow.OneSignal=window.OneSignal||[];\nOneSignal.push(function(){OneSignal.setDefaultNotificationUrl(\"https:\/\/www.vinabook.com\");var a={};window._oneSignalInitOptions=a;OneSignal.getTags(function(a){var b=\"",["escape",["macro",22],7],"\";\"undefined\"!=typeof a.UserID\u0026\u0026null!=a.UserID\u0026\u0026a.UserID!=b\u0026\u0026OneSignal.sendTag(\"UserID\",b);b=\"",["escape",["macro",23],7],"\";\"undefined\"!=typeof a.UserFirstName\u0026\u0026null!=a.UserFirstName\u0026\u0026a.UserFirstName!=b\u0026\u0026OneSignal.sendTag(\"UserFirstName\",b);b=\"",["escape",["macro",24],7],"\";\"undefined\"!=typeof a.UserEmail\u0026\u0026null!=a.UserEmail\u0026\u0026a.UserEmail!=\nb\u0026\u0026OneSignal.sendTag(\"UserEmail\",b);b=\"",["escape",["macro",25],7],"\";\"undefined\"!=typeof a.UserLocation\u0026\u0026null!=a.UserLocation\u0026\u0026a.UserLocation!=b\u0026\u0026OneSignal.sendTag(\"UserLocation\",b)});a.appId=\"7c4beead-13b6-416a-a8df-8c43c15f9cff\";a.autoRegister=!0;a.welcomeNotification={};a.welcomeNotification.title=\"Vinabook\";a.welcomeNotification.message=\"C\\u1ea3m \\u01a1n b\\u1ea1n \\u0111\\u00e3 \\u0111\\u0103ng k\\u00fd nh\\u1eadn tin t\\u1eeb Vinabook\";a.notifyButton={};a.notifyButton.enable=!1;a.notifyButton.position=\"bottom-right\";\na.notifyButton.theme=\"default\";a.notifyButton.size=\"medium\";a.notifyButton.prenotify=!1;a.notifyButton.displayPredicate=function(){return OneSignal.isPushNotificationsEnabled().then(function(a){return!a})};a.notifyButton.showCredit=!0;a.notifyButton.text={};a.notifyButton.text[\"message.prenotify\"]=\"\\u0110\\u0103ng k\\u00fd nh\\u1eadn tin t\\u1eeb Vinabook\";a.notifyButton.text[\"tip.state.unsubscribed\"]=\"H\\u1ee7y \\u0111\\u0103ng k\\u00fd nh\\u1eadn tin t\\u1eeb Vinabook\";a.notifyButton.text[\"tip.state.subscribed\"]=\n\"B\\u1ea1n \\u0111\\u00e3 \\u0111\\u0103ng k\\u00fd nh\\u1eadn tin\";a.notifyButton.text[\"tip.state.blocked\"]=\"B\\u1ea1n \\u0111\\u00e3 ch\\u1eb7n nh\\u1eadn tin\";a.notifyButton.text[\"message.action.subscribed\"]=\"C\\u1ea3m \\u01a1n b\\u1ea1n \\u0111\\u00e3 \\u0111\\u0103ng k\\u00fd nh\\u1eadn tin t\\u1eeb Vinabook\";a.notifyButton.text[\"message.action.resubscribed\"]=\"B\\u1ea1n \\u0111\\u00e3 \\u0111\\u0103ng k\\u00fd l\\u1ea1i\";a.notifyButton.text[\"message.action.unsubscribed\"]=\"B\\u1ea1n \\u0111\\u00e3 g\\u1ee1 b\\u1ecf \\u0111\\u0103ng k\\u00fd\";\na.notifyButton.text[\"dialog.main.title\"]=\"Qu\\u1ea3n l\\u00fd b\\u1ea3n tin\";a.notifyButton.text[\"dialog.main.button.subscribe\"]=\"\\u0110\\u0103ng k\\u00fd\";a.notifyButton.text[\"dialog.main.button.unsubscribe\"]=\"G\\u1ee1 \\u0111\\u0103ng k\\u00fd\";a.notifyButton.text[\"dialog.blocked.title\"]=\"M\\u1edf kh\\u00f3a \\u0111\\u0103ng k\\u00fd\";a.notifyButton.text[\"dialog.blocked.message\"]=\"L\\u00e0m theo h\\u01b0\\u1edbng d\\u1eabn sau \\u0111\\u1ec3 k\\u00edch ho\\u1ea1t \\u0111\\u0103ng k\\u00fd\";OneSignal.init(window._oneSignalInitOptions)});\nfunction documentInitOneSignal(){for(var a=document.getElementsByClassName(\"OneSignal-prompt\"),c=function(a){OneSignal.push([\"registerForPushNotifications\"]);a.preventDefault()},b=0;b\u003Ca.length;b++)a[b].addEventListener(\"click\",c,!1)}\"complete\"===document.readyState?documentInitOneSignal():window.addEventListener(\"load\",function(a){documentInitOneSignal()});\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":4
    },{
      "function":"__html",
      "once_per_load":true,
      "vtp_html":["template","\u003Cimg src=\"",["escape",["macro",26],14,3],"\" width=\"1\" height=\"1\" style=\"display: none;\"\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":7
    },{
      "function":"__html",
      "setup_tags":["list",["tag",78,1]],
      "once_per_load":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003Evar catID=\"",["escape",["macro",27],7],"\",catIDs=[];\"undefined\"!=typeof catID\u0026\u0026\"\"!=catID\u0026\u0026OneSignal.getTags(function(a){\"undefined\"!=typeof a.LastViewCatIDs\u0026\u0026null!=a.LastViewCatIDs\u0026\u0026(catIDs=a.LastViewCatIDs.split(\",\").map(Number));catIDs.push(catID);catIDs=catIDs.filter(function(a,b,c){return c.indexOf(a)==b});5\u003CcatIDs.length\u0026\u0026catIDs.shift();OneSignal.sendTag(\"LastViewCatIDs\",catIDs.join(\",\"))});\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":9
    },{
      "function":"__html",
      "setup_tags":["list",["tag",78,1]],
      "once_per_load":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003EOneSignal.sendTag(\"LastPurchasedDate\",\"",["escape",["macro",28],7],"\");OneSignal.getTags(function(a){var b=",["escape",["macro",29],8,16],";if(\"undefined\"!=typeof a.PurchasedCateIds\u0026\u0026null!=a.PurchasedCateIds){a=a.PurchasedCateIds.split(\",\").map(Number);var c;for(c=0;c\u003Ca.length;++c)b.push(a[c])}b=b.filter(function(a,b,c){return c.indexOf(a)==b});OneSignal.sendTag(\"PurchasedCateIds\",b.join(\",\"))});\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":10
    },{
      "function":"__html",
      "metadata":["map"],
      "once_per_event":true,
      "vtp_html":["template","\n\t\u003Cscript type=\"text\/gtmscript\"\u003E(function(a,e,f,g,b,c,d){a.EmaticsObject=b;a[b]=a[b]||function(){(a[b].q=a[b].q||[]).push(arguments)};a[b].l=1*new Date;c=e.createElement(f);d=e.getElementsByTagName(f)[0];c.async=1;c.src=g;d.parentNode.insertBefore(c,d)})(window,document,\"script\",\"\/\/api.ematicsolutions.com\/v1\/ematic.min.js\",\"ematics\");\nvar ematicApikey=\"15662c14f05711e782460242ac110002-sg7\",vinaemail=0\u003Cdocument.getElementsByClassName(\"cm-dialog-opener cm-dialog-auto-size account\").length?\"\":\"",["escape",["macro",24],7],"\",opt={email:vinaemail,country_iso:\"vietnam\",currency_iso:\"vnd\",language_iso:\"vietnam\"};ematics(\"create\",ematicApikey,opt);\nif(0==document.getElementsByClassName(\"cm-dialog-opener cm-dialog-auto-size account\").length){vinaemail=\"",["escape",["macro",24],7],"\";var callback=function(a){console.log(a)};ematics(\"subscribe\",\"\",vinaemail,{},callback)};\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":11
    },{
      "function":"__html",
      "metadata":["map"],
      "setup_tags":["list",["tag",82,1]],
      "once_per_load":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003Evar ordersProducts=",["escape",["macro",30],8,16],",e_products=[];$.each(ordersProducts,function(b,a){1!=a.productName.includes(\"Qu\\u00e0 T\\u1eb7ng\")\u0026\u0026e_products.push({id:a.productID,price:a.price,priceNumber:Number(a.price.replace(\",\",\"\").replace(\",\",\"\").replace(\",\",\"\")).toFixed(3),misc1:a.listPrice,quantity:a.quantity,name:a.productName,brandName:a.publisher,imageUrl:a.productImage,link:a.productLink})});ematics(\"log\",\"product\",\"convert\",e_products);\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":12
    },{
      "function":"__html",
      "metadata":["map"],
      "setup_tags":["list",["tag",82,1]],
      "once_per_load":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003Evar cartProducts=",["escape",["macro",31],8,16],",e_products=[];$.each(cartProducts,function(b,a){1!=a.productName.includes(\"Qu\\u00e0 T\\u1eb7ng\")\u0026\u0026e_products.push({id:a.productID,price:a.price,priceNumber:Number(a.price.replace(\",\",\"\").replace(\",\",\"\").replace(\",\",\"\")).toFixed(3),misc1:a.listPrice,quantity:a.quantity,name:a.productName,brandName:a.publisher,imageUrl:a.productImage,link:a.productLink})});ematics(\"log\",\"product\",\"checkout\",e_products);\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":13
    },{
      "function":"__html",
      "metadata":["map"],
      "setup_tags":["list",["tag",82,0]],
      "once_per_load":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003Evar products=",["escape",["macro",32],8,16],",e_products=[];$.each(products,function(b,a){e_products.push({id:a.productID,price:a.price,priceNumber:Number(a.price.replace(\",\",\"\").replace(\",\",\"\").replace(\",\",\"\")).toFixed(3),quantity:1,priceCurrency:a.priceCurrency,misc1:a.listPrice,name:a.productName,brandName:a.publisher,imageUrl:a.productImage,link:a.productLink})});ematics(\"log\",\"product\",\"browse\",e_products);\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":14
    },{
      "function":"__html",
      "once_per_load":true,
      "vtp_html":"\n\u003Cscript type=\"text\/gtmscript\"\u003E!function(b,e,f,g,a,c,d){b.fbq||(a=b.fbq=function(){a.callMethod?a.callMethod.apply(a,arguments):a.queue.push(arguments)},b._fbq||(b._fbq=a),a.push=a,a.loaded=!0,a.version=\"2.0\",a.queue=[],c=e.createElement(f),c.async=!0,c.src=g,d=e.getElementsByTagName(f)[0],d.parentNode.insertBefore(c,d))}(window,document,\"script\",\"\/\/connect.facebook.net\/en_US\/fbevents.js\");fbq(\"init\",\"591217300967153\");fbq(\"track\",\"PageView\");\u003C\/script\u003E\n\u003Cnoscript\u003E\u003Cimg height=\"1\" width=\"1\" style=\"display:none\" src=\"https:\/\/www.facebook.com\/tr?id=591217300967153\u0026amp;ev=PageView\u0026amp;noscript=1\"\u003E\u003C\/noscript\u003E\n",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":15
    },{
      "function":"__html",
      "setup_tags":["list",["tag",86,1]],
      "once_per_load":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003Evar productIds=",["escape",["macro",34],8,16],"||[],productPrice=",["escape",["macro",35],8,16],"||\"\";fbq(\"track\",\"ViewContent\",{content_ids:productIds,content_type:\"product\",value:productPrice.toString().replace(\/,\/g,\".\"),currency:\"VND\"});\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":16
    },{
      "function":"__html",
      "setup_tags":["list",["tag",86,1]],
      "once_per_load":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003Evar cartSubtotal=\"",["escape",["macro",36],7],"\";fbq(\"track\",\"AddToCart\",{content_ids:",["escape",["macro",34],8,16],",content_type:\"product\",value:cartSubtotal.toString().replace(\/,\/g,\".\"),currency:\"VND\"});\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":17
    },{
      "function":"__html",
      "setup_tags":["list",["tag",86,1]],
      "once_per_load":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003Evar ordersDetail=",["escape",["macro",37],8,16],";fbq(\"track\",\"Purchase\",{content_ids:",["escape",["macro",34],8,16],",content_type:\"product\",value:ordersDetail.OrdersTotal.toString().replace(\/,\/g,\".\"),currency:\"VND\"});\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":18
    },{
      "function":"__html",
      "once_per_load":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Ewindow.___gcfg={lang:\"vi\"};(function(){var a=document.createElement(\"script\");a.type=\"text\/javascript\";a.async=!0;a.src=\"https:\/\/apis.google.com\/js\/platform.js\";var b=document.getElementsByTagName(\"script\")[0];b.parentNode.insertBefore(a,b)})();(function(){window._fbds=window._fbds||{};_fbds.pixelId=591217300967153;var a=document.createElement(\"script\");a.async=!0;a.src=\"\/\/connect.facebook.net\/en_US\/fbds.js\";var b=document.getElementsByTagName(\"script\")[0];b.parentNode.insertBefore(a,b)})();\nwindow._fbq=window._fbq||[];window._fbq.push([\"track\",\"PixelInitialized\",{}]);\u003C\/script\u003E\n\u003Cnoscript\u003E\u003Cimg height=\"1\" width=\"1\" border=\"0\" alt=\"\" style=\"display:none\" src=\"https:\/\/www.facebook.com\/tr?id=591217300967153\u0026amp;ev=PixelInitialized\"\u003E\u003C\/noscript\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":19
    },{
      "function":"__html",
      "once_per_load":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003E!function(){var a=window.analytics=window.analytics||[];if(!a.initialize)if(a.invoked)window.console\u0026\u0026console.error\u0026\u0026console.error(\"Segment snippet included twice.\");else{a.invoked=!0;a.methods=\"trackSubmit trackClick trackLink trackForm pageview identify group track ready alias page once off on\".split(\" \");a.factory=function(e){return function(){var b=Array.prototype.slice.call(arguments);b.unshift(e);a.push(b);return a}};for(var c=0;c\u003Ca.methods.length;c++){var d=a.methods[c];a[d]=a.factory(d)}a.load=\nfunction(a){var b=document.createElement(\"script\");b.type=\"text\/javascript\";b.async=!0;b.src=(\"https:\"===document.location.protocol?\"https:\/\/\":\"http:\/\/\")+\"cdn.segment.com\/analytics.js\/v1\/\"+a+\"\/analytics.min.js\";a=document.getElementsByTagName(\"script\")[0];a.parentNode.insertBefore(b,a)};a.SNIPPET_VERSION=\"3.0.1\";a.load(\"9fZymSgO5NCbqizjDk264ONzT2uvGRfK\");a.page()}}();\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":20
    },{
      "function":"__html",
      "setup_tags":["list",["tag",91,1]],
      "once_per_load":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003E$(document).ready(function(){var b=",["escape",["macro",22],8,16],";if(\"undefined\"!==b){var a={},c=\"",["escape",["macro",23],7],"\",d=\"",["escape",["macro",24],7],"\",e=\"",["escape",["macro",38],7],"\",f=\"",["escape",["macro",39],7],"\";\"undefined\"!==c\u0026\u0026(a.name=c);\"undefined\"!==d\u0026\u0026(a.email=d);\"undefined\"!==e\u0026\u0026(a.birthdate=e);\"undefined\"!==f\u0026\u0026(a.gender=2==f?\"male\":\"female\");0\u003Ca.length\u0026\u0026analytics.identify(b,a)}});\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":21
    },{
      "function":"__html",
      "setup_tags":["list",["tag",92,1]],
      "once_per_load":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003E$(document).ready(function(){var a={},b=",["escape",["macro",22],8,16],",c=\"",["escape",["macro",40],7],"\";\"undefined\"!==b\u0026\u0026(a.userId=b);\"undefined\"!==c\u0026\u0026(a.registerdate=c);0\u003Ca.length\u0026\u0026analytics.track(\"Logged In\",a)});\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":22
    },{
      "function":"__html",
      "setup_tags":["list",["tag",92,1]],
      "once_per_load":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003E$(document).ready(function(){var a=",["escape",["macro",22],8,16],";\"undefined\"!==a\u0026\u0026analytics.track(\"Logged Out\",{userId:a})});\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":23
    },{
      "function":"__html",
      "setup_tags":["list",["tag",92,1]],
      "once_per_load":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003E$(document).ready(function(){var a={},b=",["escape",["macro",22],8,16],",c=\"",["escape",["macro",40],7],"\",d=\"",["escape",["macro",23],7],"\",e=\"",["escape",["macro",24],7],"\",f=\"",["escape",["macro",38],7],"\",g=\"",["escape",["macro",39],7],"\";\"undefined\"!==b\u0026\u0026(a.userId=b);\"undefined\"!==c\u0026\u0026(a.registerdate=c);\"undefined\"!==d\u0026\u0026(a.name=d);\"undefined\"!==e\u0026\u0026(a.email=e);\"undefined\"!==f\u0026\u0026(a.birthdate=f);\"undefined\"!==g\u0026\u0026(a.gender=2==g?\"male\":\"female\");0\u003Ca.length\u0026\u0026analytics.track(\"Signed Up\",a)});\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":24
    },{
      "function":"__html",
      "setup_tags":["list",["tag",91,0]],
      "once_per_load":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003E$(document).ready(function(){var b={},a=",["escape",["macro",41],8,16],";\"undefined\"!==typeof a\u0026\u0026(\"undefined\"!==typeof a.productID\u0026\u0026\"\"!=a.productID\u0026\u0026(b.id=a.productID,b.sku=a.productID),\"undefined\"!==typeof a.productName\u0026\u0026\"\"!=a.productName\u0026\u0026(b.name=a.productName),\"undefined\"!==typeof a.price\u0026\u0026\"\"!=a.price\u0026\u0026(b.price=parseInt(a.price.replace(\",\",\"\"))),\"undefined\"!==typeof a.category\u0026\u0026\"\"!=a.category\u0026\u0026(b.category=a.category),\"undefined\"!==typeof b\u0026\u0026analytics.track(\"Viewed Product\",b))});\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":25
    },{
      "function":"__html",
      "setup_tags":["list",["tag",92,1]],
      "once_per_load":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003E$(document).ready(function(){var a=\"",["escape",["macro",42],7],"\";\"undefined\"!==a\u0026\u0026analytics.track(\"Subscribed to Newsletter\",{email:a})});\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":26
    },{
      "function":"__html",
      "setup_tags":["list",["tag",92,1]],
      "once_per_load":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003E$(document).ready(function(){var d=",["escape",["macro",22],8,16],",a=",["escape",["macro",37],8,16],",e=",["escape",["macro",30],8,16],";if(\"undefined\"!==d){var c={};\"undefined\"!==typeof a.OrdersID\u0026\u0026\"\"!=a.OrdersID\u0026\u0026(c.orderId=a.OrdersID,\"undefined\"!==typeof a.OrdersSubTotal\u0026\u0026\"\"!=a.OrdersSubTotal\u0026\u0026(c.subTotal=parseInt(a.OrdersSubTotal.replace(\",\",\"\"))),\"undefined\"!==typeof a.OrdersPaidTotal\u0026\u0026\"\"!=a.OrdersPaidTotal\u0026\u0026(c.paidTotal=parseInt(a.OrdersPaidTotal.replace(\",\",\"\"))),\"undefined\"!==typeof a.OrdersTotal\u0026\u0026\"\"!=a.OrdersTotal\u0026\u0026(c.total=\nparseInt(a.OrdersTotal.replace(\",\",\"\"))),c.userId=d,analytics.track(\"Completed Checkout Step\",c),0\u003Ce.length\u0026\u0026$.each(e,function(c,e){var b={};b.orderId=a.OrdersID;b.userId=d;\"undefined\"!==typeof a.productID\u0026\u0026\"\"!=a.productID\u0026\u0026(b.productId=a.productID);\"undefined\"!==typeof a.productName\u0026\u0026\"\"!=a.productName\u0026\u0026(b.name=a.productName);\"undefined\"!==typeof a.quantity\u0026\u0026\"\"!=a.quantity\u0026\u0026(b.amount=a.quantity);\"undefined\"!==typeof a.price\u0026\u0026\"\"!=a.price\u0026\u0026(b.price=parseInt(a.price.replace(\",\",\"\")));\"undefined\"!==typeof a.categoryName\u0026\u0026\n\"\"!=a.categoryName\u0026\u0026(b.category=a.categoryName);analytics.track(\"Completed Checkout Product\",b)}))}});\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":27
    },{
      "function":"__html",
      "setup_tags":["list",["tag",92,1]],
      "once_per_load":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003E$(document).ready(function(){var c=",["escape",["macro",32],8,16],";\"undefined\"!==typeof c\u0026\u00260\u003Cc.length\u0026\u0026$.each(c,function(c,a){var b={};\"undefined\"!==typeof a.productID\u0026\u0026\"\"!=a.productID\u0026\u0026(b.id=a.productID,b.sku=a.productID);\"undefined\"!==typeof a.productName\u0026\u0026\"\"!=a.productName\u0026\u0026(b.name=a.productName);\"undefined\"!==typeof a.price\u0026\u0026\"\"!=a.price\u0026\u0026(b.price=parseInt(a.price.replace(\",\",\"\")));\"undefined\"!==typeof a.category\u0026\u0026\"\"!=a.category\u0026\u0026(b.category=a.category);0\u003Cb.length\u0026\u0026analytics.track(\"Added Product\",b)})});\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":28
    },{
      "function":"__html",
      "once_per_load":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003Evar ordersDetail=",["escape",["macro",37],8,16],";(function(){var b=window._fbq||(window._fbq=[]);if(!b.loaded){var a=document.createElement(\"script\");a.async=!0;a.src=\"\/\/connect.facebook.net\/en_US\/fbds.js\";var c=document.getElementsByTagName(\"script\")[0];c.parentNode.insertBefore(a,c);b.loaded=!0}})();window._fbq=window._fbq||[];window._fbq.push([\"track\",\"6033216385876\",{value:ordersDetail.OrdersTotal,currency:\"VND\"}]);\u003C\/script\u003E\n\n\u003Cnoscript\u003E\n  \t\u003Cimg height=\"1\" width=\"1\" alt=\"\" style=\"display:none\" src=\"",["escape",["macro",43],14,3],"\"\u003E\n\u003C\/noscript\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":29
    },{
      "function":"__html",
      "setup_tags":["list",["tag",0,1]],
      "once_per_load":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003Evar ordersDetail=",["escape",["macro",37],8,16],",_gaq=_gaq||[];_gaq.push([\"_set\",\"currencyCode\",\"VND\"]);_gaq.push([\"_setAccount\",\"UA-2029810-2\"]);_gaq.push([\"_setDomainName\",\"vinabook.com\"]);_gaq.push([\"_trackPageview\"]);_gaq.push([\"_addTrans\",ordersDetail.OrdersID,\"Hotdeal.vn\",parseInt(ordersDetail.OrdersTotal.replace(\",\",\"\")),parseInt(ordersDetail.Tax.replace(\",\",\"\")),parseInt(ordersDetail.ShippingCost.replace(\",\",\"\")),ordersDetail.OrdersStateDes,ordersDetail.OrdersStateDes,ordersDetail.OrdersCountryDes]);\nvar ordersProducts=",["escape",["macro",30],8,16],"||[];$.each(ordersProducts,function(c,a){var b=parseInt(a.unitPrice.replace(\",\",\"\"));_gaq.push([\"_addItem\",ordersDetail.OrdersID,a.productCode,a.productName,a.categoryName,b,a.quantity])});_gaq.push([\"_trackTrans\"]);\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":30
    },{
      "function":"__html",
      "metadata":["map"],
      "setup_tags":["list",["tag",82,1]],
      "once_per_load":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003Evar cartProducts=",["escape",["macro",31],8,16],",e_products=[];$.each(cartProducts,function(b,a){1!=a.productName.includes(\"Qu\\u00e0 T\\u1eb7ng\")\u0026\u0026e_products.push({id:a.productID,price:a.price,priceNumber:Number(a.price.replace(\",\",\"\").replace(\",\",\"\").replace(\",\",\"\")).toFixed(3),misc1:a.listPrice,quantity:a.quantity,name:a.productName,brandName:a.publisher,imageUrl:a.productImage,link:a.productLink})});ematics(\"log\",\"product\",\"cart\",e_products);\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":69
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\" async=\"async\" data-gtmsrc=\"",["escape",["macro",44],14,3],"\"\u003E\u003C\/script\u003E\n\u003Cnoscript\u003E\n  \u003Ciframe src=\"",["escape",["macro",44],14,3],"\" width=\"1\" height=\"1\"\u003E\u003C\/iframe\u003E\n\u003C\/noscript\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":70
    },{
      "function":"__html",
      "metadata":["map"],
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003E(function(){var b=document.getElementsByClassName(\"form-wrapper\")[0];b.addEventListener(\"submit\",function(a){a.preventDefault();a=function(a){console.log(a);b.submit()};if(document.getElementsByClassName(\"form-control txt-email\")[0].value.includes(\"@\")){var c=document.getElementsByClassName(\"form-control txt-email\")[0].value.replace(\"+fb-thefaceshopvn\",\"\").replace(\"+gg-thefaceshopvn\",\"\");ematics(\"subscribe\",\"\",c,{},a)}else b.submit()})})();\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":71
    },{
      "function":"__html",
      "metadata":["map"],
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Evar targetNode=document.getElementsByClassName(\"dropdown-box-v5\")[0],config={attributes:!0,childList:!0,subtree:!0},callback=function(c,d){setTimeout(function(){if(-1===window.location.href.indexOf(\"checkout\")){var a=[];if(0\u003Cdocument.getElementsByClassName(\"cart-items\")[0].childNodes[1].children.length){var b=document.getElementsByClassName(\"cart-items\")[0].childNodes[1].children[0].children.length;console.log(\"Cart Length: \"+b);for(i=0;i\u003Cb;i++)1!=document.getElementsByClassName(\"cart-items\")[0].childNodes[1].children[0].children[i].children[1].children[0].innerText.includes(\"Qu\\u00e0 T\\u1eb7ng\")\u0026\u0026\n(a.push({id:document.getElementsByClassName(\"cart-items\")[0].childNodes[1].children[0].children[i].children[1].children[0].href.split(\"-p\")[1].split(\".\")[0],price:document.getElementsByClassName(\"cart-items\")[0].childNodes[1].children[0].children[i].children[1].children[1].children[2].innerText.trim(),priceNumber:Number(document.getElementsByClassName(\"cart-items\")[0].childNodes[1].children[0].children[i].children[1].children[1].children[2].innerText.replace(\"\\u00a0\\u20ab\",\"\").replace(\".\",\"\").replace(\".\",\n\"\")).toFixed(3),priceCurrency:\"VND\",name:document.getElementsByClassName(\"cart-items\")[0].childNodes[1].children[0].children[i].children[1].children[0].innerText,quantity:Number(document.getElementsByClassName(\"cart-items\")[0].childNodes[1].children[0].children[i].children[1].children[1].children[0].innerText),brandName:\"Vinabook Vietnam\",desc:\"\",imageUrl:document.getElementsByClassName(\"cart-items\")[0].childNodes[1].children[0].children[i].children[0].children[0].currentSrc.replace(\"40x0\",\"240x\"),\nlink:document.getElementsByClassName(\"cart-items\")[0].childNodes[1].children[0].children[i].children[1].children[0].href}),console.log(\"Iteration: \"+i))}else a=[];ematics(\"log\",\"product\",\"cart\",a);sessionStorage.setItem(\"product\",JSON.stringify(a))}},1500)},observer=new MutationObserver(callback);observer.observe(targetNode,config);\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":72
    }],
  "predicates":[{
      "function":"_eq",
      "arg0":["macro",5],
      "arg1":"gtm.js"
    },{
      "function":"_re",
      "arg0":["macro",8],
      "arg1":"index.php.*[\\?\\\u0026]dispatch=checkout.checkout.*"
    },{
      "function":"_eq",
      "arg0":["macro",5],
      "arg1":"gtm.dom"
    },{
      "function":"_re",
      "arg0":["macro",8],
      "arg1":"index.php.*[\\?\\\u0026]dispatch=checkout.complete.*"
    },{
      "function":"_eq",
      "arg0":["macro",9],
      "arg1":"search-form-header"
    },{
      "function":"_eq",
      "arg0":["macro",5],
      "arg1":"gtm.formSubmit"
    },{
      "function":"_re",
      "arg0":["macro",10],
      "arg1":"(^$|((^|,)10569849_127($|,)))"
    },{
      "function":"_css",
      "arg0":["macro",3],
      "arg1":".product-top-week \u003E div.mainbox-body \u003E ul \u003E form \u003E li \u003E div.pic-thumb \u003E a \u003E img,.product-top-week \u003E div.mainbox-body \u003E ul \u003E form \u003E li \u003E div.info-book-r \u003E div.title-book-r \u003E a"
    },{
      "function":"_eq",
      "arg0":["macro",5],
      "arg1":"gtm.click"
    },{
      "function":"_css",
      "arg0":["macro",3],
      "arg1":".product-best-seller \u003E div.mainbox-body \u003E div.box-info-wrapper \u003E div \u003E form \u003E div.box-info-book \u003E div.pic-book \u003E a \u003E img,.product-best-seller \u003E div.mainbox-body \u003E div.box-info-wrapper \u003E div \u003E form \u003E div.box-info-book \u003E div.text-info-book \u003E div.box-title-book \u003E div.title-book-small \u003E a"
    },{
      "function":"_css",
      "arg0":["macro",3],
      "arg1":".big-banner \u003E a \u003E img, #vinabook-carousel \u003E div \u003E div.item.active \u003E a \u003E img"
    },{
      "function":"_css",
      "arg0":["macro",3],
      "arg1":".small-banner \u003E div.banner-t \u003E a \u003E img"
    },{
      "function":"_css",
      "arg0":["macro",3],
      "arg1":".small-banner \u003E div.banner-b \u003E a \u003E img"
    },{
      "function":"_css",
      "arg0":["macro",3],
      "arg1":".small-banner-bottom.box-thumb \u003E div:nth-child(1) \u003E a \u003E img"
    },{
      "function":"_css",
      "arg0":["macro",3],
      "arg1":".small-banner-bottom.box-thumb \u003E div:nth-child(2) \u003E a \u003E img"
    },{
      "function":"_css",
      "arg0":["macro",3],
      "arg1":".small-banner-bottom.box-thumb \u003E div:nth-child(3) \u003E a \u003E img"
    },{
      "function":"_css",
      "arg0":["macro",3],
      "arg1":".product-newspapers-introduce \u003E div.mainbox-body \u003E ul \u003E form \u003E li \u003E div.pic-thumb \u003E a \u003E img,.product-newspapers-introduce \u003E div.mainbox-body \u003E ul \u003E form \u003E li \u003E div.info-book-r \u003E div.title-book-r \u003E a"
    },{
      "function":"_css",
      "arg0":["macro",3],
      "arg1":".product-new-imported \u003E div.mainbox-body \u003E ul \u003E form \u003E li \u003E div.pic-thumb \u003E a \u003E img,.product-new-imported \u003E div.mainbox-body \u003E ul \u003E form \u003E li \u003E div.info-book-r \u003E div.title-book-r \u003E a"
    },{
      "function":"_css",
      "arg0":["macro",3],
      "arg1":".block-truyen-ke-cho-be \u003E div.mainbox-body \u003E ul \u003E form \u003E li \u003E div.pic-thumb \u003E a \u003E img,.block-truyen-ke-cho-be \u003E div.mainbox-body \u003E ul \u003E form \u003E li \u003E div.info-book-r \u003E div.title-book-r \u003E a"
    },{
      "function":"_css",
      "arg0":["macro",3],
      "arg1":".block-highlight-author \u003E div.mainbox-body \u003E div.book-tac-gia \u003E div.flex-wrapper \u003E div.img-thumb \u003E a \u003E img,.block-highlight-author \u003E div.mainbox-body \u003E div.book-tac-gia \u003E div.flex-wrapper \u003E div.text-content-tac-gia \u003E div.title-tac-gia \u003E a,.block-highlight-author \u003E div.mainbox-body \u003E div.book-tac-gia \u003E div.flex-wrapper \u003E div.text-content-tac-gia \u003E div.text-info-tac-gia \u003E a"
    },{
      "function":"_eq",
      "arg0":["macro",5],
      "arg1":"gtm.video"
    },{
      "function":"_re",
      "arg0":["macro",10],
      "arg1":"(^$|((^|,)10569849_161($|,)))"
    },{
      "function":"_css",
      "arg0":["macro",3],
      "arg1":".block-highlight-author \u003E div.mainbox-body \u003E div.book-tac-gia \u003E div.more-tac-gia \u003E div.product-by-tac-gia \u003E div \u003E div \u003E figure \u003E a \u003E img,.block-highlight-author \u003E div.mainbox-body \u003E div.book-tac-gia \u003E div.more-tac-gia \u003E div.product-by-tac-gia \u003E div \u003E div \u003E div \u003E a"
    },{
      "function":"_css",
      "arg0":["macro",3],
      "arg1":"#search-autocomplete \u003E ul \u003E li \u003E a,#search-autocomplete \u003E ul \u003E li \u003E a \u003E div.image-thumb-min \u003E img,#search-autocomplete \u003E ul \u003E li \u003E a \u003E div.suggestionlabel \u003E span"
    },{
      "function":"_css",
      "arg0":["macro",3],
      "arg1":".categories-wap \u003E div \u003E nav \u003E ul \u003E li \u003E ul \u003E li \u003E a"
    },{
      "function":"_eq",
      "arg0":["macro",15],
      "arg1":"read_more"
    },{
      "function":"_css",
      "arg0":["macro",3],
      "arg1":"a.image-border"
    },{
      "function":"_eq",
      "arg0":["macro",17],
      "arg1":"page-products-search"
    },{
      "function":"_css",
      "arg0":["macro",3],
      "arg1":".home-block-recent \u003E div.mainbox-body \u003E div.box-info-wrapper \u003E div \u003E form \u003E div.box-info-book \u003E div.pic-book \u003E a \u003E img,.home-block-recent \u003E div.mainbox-body \u003E div.box-info-wrapper \u003E div \u003E form \u003E div.box-info-book \u003E div.text-info-book \u003E div.box-title-book \u003E div \u003E a"
    },{
      "function":"_css",
      "arg0":["macro",3],
      "arg1":".breadcrumbs \u003E li \u003E a \u003E span"
    },{
      "function":"_css",
      "arg0":["macro",3],
      "arg1":"a.image-border,a.product-title,a.readmore"
    },{
      "function":"_re",
      "arg0":["macro",17],
      "arg1":"(page-authors-view|page-publishers-view)"
    },{
      "function":"_css",
      "arg0":["macro",3],
      "arg1":"a.author"
    },{
      "function":"_eq",
      "arg0":["macro",17],
      "arg1":"page-products-view"
    },{
      "function":"_css",
      "arg0":["macro",3],
      "arg1":"a.publishers"
    },{
      "function":"_eq",
      "arg0":["macro",17],
      "arg1":"page-categories-view"
    },{
      "function":"_re",
      "arg0":["macro",18],
      "arg1":"^dispatch\\[checkout\\.add\\.\\."
    },{
      "function":"_eq",
      "arg0":["macro",18],
      "arg1":"dispatch[checkout.adds]"
    },{
      "function":"_css",
      "arg0":["macro",3],
      "arg1":".product_recommend-box.product-details-box.sach-mua-kem \u003E div \u003E div.mainbox2-body \u003E table \u003E tbody \u003E tr \u003E td \u003E div \u003E div \u003E a,.product_recommend-box.product-details-box.sach-mua-kem \u003E div \u003E div.mainbox2-body \u003E div \u003E div \u003E label \u003E a"
    },{
      "function":"_css",
      "arg0":["macro",3],
      "arg1":".categories-wap \u003E div \u003E nav \u003E ul \u003E li \u003E a \u003E span \u003E i"
    },{
      "function":"_css",
      "arg0":["macro",3],
      "arg1":".page-categories-left \u003E div.category-star-top \u003E div \u003E ul \u003E li \u003E a"
    },{
      "function":"_css",
      "arg0":["macro",3],
      "arg1":".page-categories-left \u003E div:nth-child(2) \u003E ul \u003E li \u003E a.category"
    },{
      "function":"_css",
      "arg0":["macro",3],
      "arg1":".page-categories-left \u003E div:nth-child(3) \u003E ul \u003E li \u003E a.author"
    },{
      "function":"_css",
      "arg0":["macro",3],
      "arg1":".page-categories-left \u003E div:nth-child(4) \u003E ul \u003E li \u003E a.publishers"
    },{
      "function":"_css",
      "arg0":["macro",3],
      "arg1":".bottom-common-search \u003E div.mainbox-body \u003E ul \u003E li \u003E a"
    },{
      "function":"_eq",
      "arg0":["macro",5],
      "arg1":"gtm.load"
    },{
      "function":"_re",
      "arg0":["macro",8],
      "arg1":"\\\/c[0-9]+\\\/.*?"
    },{
      "function":"_re",
      "arg0":["macro",8],
      "arg1":"(.*)\\.html"
    },{
      "function":"_re",
      "arg0":["macro",8],
      "arg1":"(.*)-p[0-9]+\\.html"
    },{
      "function":"_re",
      "arg0":["macro",8],
      "arg1":"\\\/tac-gia\\\/.*?"
    },{
      "function":"_re",
      "arg0":["macro",8],
      "arg1":"index.php"
    },{
      "function":"_re",
      "arg0":["macro",8],
      "arg1":"\\\/nha-phat-hanh\\\/.*?"
    },{
      "function":"_re",
      "arg0":["macro",33],
      "arg1":"^\\\/(\\?(.*))?$"
    },{
      "function":"_re",
      "arg0":["macro",8],
      "arg1":"\\\/tin-tuc\\\/.*?"
    },{
      "function":"_re",
      "arg0":["macro",8],
      "arg1":"index.php.*[\\?\\\u0026]dispatch=checkout.cart.*"
    }],
  "rules":[
    [["if",0],["add",0,1,5,76,77,86,90,91,41,42,43,44,45,46,47,48,49,50,51,52,53,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75]],
    [["if",1,2],["add",2,84,88]],
    [["if",2,3],["add",3,4,6,79,81,83,89,98,100,101,103]],
    [["if",4,5,6],["add",7]],
    [["if",7,8],["add",8]],
    [["if",8,9],["add",9]],
    [["if",8,10],["add",10]],
    [["if",8,11],["add",11]],
    [["if",8,12],["add",12]],
    [["if",8,13],["add",13]],
    [["if",8,14],["add",14]],
    [["if",8,15],["add",15]],
    [["if",8,16],["add",16]],
    [["if",8,17],["add",17]],
    [["if",8,18],["add",18]],
    [["if",8,19],["add",19]],
    [["if",20,21],["add",20]],
    [["if",8,22],["add",21]],
    [["if",8,23],["add",22]],
    [["if",8,24],["add",23]],
    [["if",8,25],["add",24]],
    [["if",8,26,27],["add",25]],
    [["if",8,28],["add",26]],
    [["if",8,29],["add",27]],
    [["if",8,30,31],["add",28]],
    [["if",8,32,33],["add",29]],
    [["if",8,33,34],["add",30]],
    [["if",8,30,35],["add",31]],
    [["if",8,33,36],["add",32]],
    [["if",8,33,37],["add",33]],
    [["if",8,38],["add",34]],
    [["if",8,39],["add",35]],
    [["if",8,40],["add",36]],
    [["if",8,41],["add",37]],
    [["if",8,42],["add",38]],
    [["if",8,43],["add",39]],
    [["if",8,44],["add",40]],
    [["if",45],["add",54,104]],
    [["if",2],["add",78,82,92,93,94,95,97,99]],
    [["if",2,46],["add",80]],
    [["if",2,47],["add",85,87,96]],
    [["if",2,47],["unless",48],["add",87]],
    [["if",2],["unless",46,47,49,50,51,52,53],["add",87]],
    [["if",2,54],["add",102]],
    [["if",8],["add",105]]]
},
"runtime":[
[],[]
]



};
var aa,ba="function"==typeof Object.create?Object.create:function(a){var b=function(){};b.prototype=a;return new b},da;if("function"==typeof Object.setPrototypeOf)da=Object.setPrototypeOf;else{var fa;a:{var ha={sf:!0},ia={};try{ia.__proto__=ha;fa=ia.sf;break a}catch(a){}fa=!1}da=fa?function(a,b){a.__proto__=b;if(a.__proto__!==b)throw new TypeError(a+" is not extensible");return a}:null}var ka=da,la=this||self,ma=/^[\w+/_-]+[=]{0,2}$/,oa=null;var pa=function(){},qa=function(a){return"function"==typeof a},f=function(a){return"string"==typeof a},ra=function(a){return"number"==typeof a&&!isNaN(a)},ua=function(a){return"[object Array]"==Object.prototype.toString.call(Object(a))},t=function(a,b){if(Array.prototype.indexOf){var c=a.indexOf(b);return"number"==typeof c?c:-1}for(var d=0;d<a.length;d++)if(a[d]===b)return d;return-1},va=function(a,b){if(a&&ua(a))for(var c=0;c<a.length;c++)if(a[c]&&b(a[c]))return a[c]},wa=function(a,b){if(!ra(a)||
!ra(b)||a>b)a=0,b=2147483647;return Math.floor(Math.random()*(b-a+1)+a)},ya=function(a,b){for(var c=new xa,d=0;d<a.length;d++)c.set(a[d],!0);for(var e=0;e<b.length;e++)if(c.get(b[e]))return!0;return!1},za=function(a,b){for(var c in a)Object.prototype.hasOwnProperty.call(a,c)&&b(c,a[c])},Aa=function(a){return Math.round(Number(a))||0},Ca=function(a){return"false"==String(a).toLowerCase()?!1:!!a},Da=function(a){var b=[];if(ua(a))for(var c=0;c<a.length;c++)b.push(String(a[c]));return b},Ea=function(a){return a?
a.replace(/^\s+|\s+$/g,""):""},Fa=function(){return(new Date).getTime()},xa=function(){this.prefix="gtm.";this.values={}};xa.prototype.set=function(a,b){this.values[this.prefix+a]=b};xa.prototype.get=function(a){return this.values[this.prefix+a]};xa.prototype.contains=function(a){return void 0!==this.get(a)};
var Ga=function(a,b,c){return a&&a.hasOwnProperty(b)?a[b]:c},Ha=function(a){var b=!1;return function(){if(!b)try{a()}catch(c){}b=!0}},Ia=function(a,b){for(var c in b)b.hasOwnProperty(c)&&(a[c]=b[c])},Ja=function(a){for(var b in a)if(a.hasOwnProperty(b))return!0;return!1},Ka=function(a,b){for(var c=[],d=0;d<a.length;d++)c.push(a[d]),c.push.apply(c,b[a[d]]||[]);return c},La=function(a,b){for(var c={},d=c,e=a.split("."),g=0;g<e.length-1;g++)d=d[e[g]]={};d[e[e.length-1]]=b;return c};/*
 jQuery v1.9.1 (c) 2005, 2012 jQuery Foundation, Inc. jquery.org/license. */
var Ma=/\[object (Boolean|Number|String|Function|Array|Date|RegExp)\]/,Na=function(a){if(null==a)return String(a);var b=Ma.exec(Object.prototype.toString.call(Object(a)));return b?b[1].toLowerCase():"object"},Oa=function(a,b){return Object.prototype.hasOwnProperty.call(Object(a),b)},Pa=function(a){if(!a||"object"!=Na(a)||a.nodeType||a==a.window)return!1;try{if(a.constructor&&!Oa(a,"constructor")&&!Oa(a.constructor.prototype,"isPrototypeOf"))return!1}catch(c){return!1}for(var b in a);return void 0===
b||Oa(a,b)},C=function(a,b){var c=b||("array"==Na(a)?[]:{}),d;for(d in a)if(Oa(a,d)){var e=a[d];"array"==Na(e)?("array"!=Na(c[d])&&(c[d]=[]),c[d]=C(e,c[d])):Pa(e)?(Pa(c[d])||(c[d]={}),c[d]=C(e,c[d])):c[d]=e}return c};
var Qa=[],Ra={"\x00":"&#0;",'"':"&quot;","&":"&amp;","'":"&#39;","<":"&lt;",">":"&gt;","\t":"&#9;","\n":"&#10;","\x0B":"&#11;","\f":"&#12;","\r":"&#13;"," ":"&#32;","-":"&#45;","/":"&#47;","=":"&#61;","`":"&#96;","\u0085":"&#133;","\u00a0":"&#160;","\u2028":"&#8232;","\u2029":"&#8233;"},Sa=function(a){return Ra[a]},Ta=/[\x00\x22\x26\x27\x3c\x3e]/g;Qa[3]=function(a){return String(a).replace(Ta,Sa)};var Xa=/[\x00\x08-\x0d\x22\x26\x27\/\x3c-\x3e\\\x85\u2028\u2029]/g,Ya={"\x00":"\\x00","\b":"\\x08","\t":"\\t","\n":"\\n","\x0B":"\\x0b",
"\f":"\\f","\r":"\\r",'"':"\\x22","&":"\\x26","'":"\\x27","/":"\\/","<":"\\x3c","=":"\\x3d",">":"\\x3e","\\":"\\\\","\u0085":"\\x85","\u2028":"\\u2028","\u2029":"\\u2029",$:"\\x24","(":"\\x28",")":"\\x29","*":"\\x2a","+":"\\x2b",",":"\\x2c","-":"\\x2d",".":"\\x2e",":":"\\x3a","?":"\\x3f","[":"\\x5b","]":"\\x5d","^":"\\x5e","{":"\\x7b","|":"\\x7c","}":"\\x7d"},bb=function(a){return Ya[a]};Qa[7]=function(a){return String(a).replace(Xa,bb)};
Qa[8]=function(a){if(null==a)return" null ";switch(typeof a){case "boolean":case "number":return" "+a+" ";default:return"'"+String(String(a)).replace(Xa,bb)+"'"}};var kb=/[\x00- \x22\x27-\x29\x3c\x3e\\\x7b\x7d\x7f\x85\xa0\u2028\u2029\uff01\uff03\uff04\uff06-\uff0c\uff0f\uff1a\uff1b\uff1d\uff1f\uff20\uff3b\uff3d]/g,lb={"\x00":"%00","\u0001":"%01","\u0002":"%02","\u0003":"%03","\u0004":"%04","\u0005":"%05","\u0006":"%06","\u0007":"%07","\b":"%08","\t":"%09","\n":"%0A","\x0B":"%0B","\f":"%0C","\r":"%0D","\u000e":"%0E","\u000f":"%0F","\u0010":"%10",
"\u0011":"%11","\u0012":"%12","\u0013":"%13","\u0014":"%14","\u0015":"%15","\u0016":"%16","\u0017":"%17","\u0018":"%18","\u0019":"%19","\u001a":"%1A","\u001b":"%1B","\u001c":"%1C","\u001d":"%1D","\u001e":"%1E","\u001f":"%1F"," ":"%20",'"':"%22","'":"%27","(":"%28",")":"%29","<":"%3C",">":"%3E","\\":"%5C","{":"%7B","}":"%7D","\u007f":"%7F","\u0085":"%C2%85","\u00a0":"%C2%A0","\u2028":"%E2%80%A8","\u2029":"%E2%80%A9","\uff01":"%EF%BC%81","\uff03":"%EF%BC%83","\uff04":"%EF%BC%84","\uff06":"%EF%BC%86",
"\uff07":"%EF%BC%87","\uff08":"%EF%BC%88","\uff09":"%EF%BC%89","\uff0a":"%EF%BC%8A","\uff0b":"%EF%BC%8B","\uff0c":"%EF%BC%8C","\uff0f":"%EF%BC%8F","\uff1a":"%EF%BC%9A","\uff1b":"%EF%BC%9B","\uff1d":"%EF%BC%9D","\uff1f":"%EF%BC%9F","\uff20":"%EF%BC%A0","\uff3b":"%EF%BC%BB","\uff3d":"%EF%BC%BD"},mb=function(a){return lb[a]};var nb=
/^(?:(?:https?|mailto):|[^&:\/?#]*(?:[\/?#]|$))/i;Qa[14]=function(a){var b=String(a);return nb.test(b)?b.replace(kb,mb):"#zSoyz"};Qa[16]=function(a){return a};var ob;
var pb=[],qb=[],rb=[],sb=[],ub=[],vb={},wb,xb,yb,zb=function(a,b){var c={};c["function"]="__"+a;for(var d in b)b.hasOwnProperty(d)&&(c["vtp_"+d]=b[d]);return c},Ab=function(a,b){var c=a["function"];if(!c)throw Error("Error: No function name given for function call.");var d=vb[c],e={},g;for(g in a)a.hasOwnProperty(g)&&0===g.indexOf("vtp_")&&(e[void 0!==d?g:g.substr(4)]=a[g]);return void 0!==d?d(e):ob(c,e,b)},Cb=function(a,b,c){c=c||[];var d={},e;for(e in a)a.hasOwnProperty(e)&&(d[e]=Bb(a[e],b,c));
return d},Db=function(a){var b=a["function"];if(!b)throw"Error: No function name given for function call.";var c=vb[b];return c?c.priorityOverride||0:0},Bb=function(a,b,c){if(ua(a)){var d;switch(a[0]){case "function_id":return a[1];case "list":d=[];for(var e=1;e<a.length;e++)d.push(Bb(a[e],b,c));return d;case "macro":var g=a[1];if(c[g])return;var h=pb[g];if(!h||b.Jc(h))return;c[g]=!0;try{var k=Cb(h,b,c);k.vtp_gtmEventId=b.id;d=Ab(k,b);yb&&(d=yb.Uf(d,k))}catch(y){b.he&&b.he(y,Number(g)),d=!1}c[g]=
!1;return d;case "map":d={};for(var l=1;l<a.length;l+=2)d[Bb(a[l],b,c)]=Bb(a[l+1],b,c);return d;case "template":d=[];for(var m=!1,n=1;n<a.length;n++){var q=Bb(a[n],b,c);xb&&(m=m||q===xb.zb);d.push(q)}return xb&&m?xb.Xf(d):d.join("");case "escape":d=Bb(a[1],b,c);if(xb&&ua(a[1])&&"macro"===a[1][0]&&xb.Dg(a))return xb.Og(d);d=String(d);for(var u=2;u<a.length;u++)Qa[a[u]]&&(d=Qa[a[u]](d));return d;case "tag":var p=a[1];if(!sb[p])throw Error("Unable to resolve tag reference "+p+".");return d={Wd:a[2],
index:p};case "zb":var r={arg0:a[2],arg1:a[3],ignore_case:a[5]};r["function"]=a[1];var v=Fb(r,b,c),w=!!a[4];return w||2!==v?w!==(1===v):null;default:throw Error("Attempting to expand unknown Value type: "+a[0]+".");}}return a},Fb=function(a,b,c){try{return wb(Cb(a,b,c))}catch(d){JSON.stringify(a)}return 2};var Gb=function(){var a=function(b){return{toString:function(){return b}}};return{ld:a("convert_case_to"),md:a("convert_false_to"),nd:a("convert_null_to"),od:a("convert_true_to"),pd:a("convert_undefined_to"),uh:a("debug_mode_metadata"),ma:a("function"),ff:a("instance_name"),hf:a("live_only"),jf:a("malware_disabled"),kf:a("metadata"),wh:a("original_vendor_template_id"),lf:a("once_per_event"),Jd:a("once_per_load"),Kd:a("setup_tags"),Ld:a("tag_id"),Md:a("teardown_tags")}}();var Hb=null,Kb=function(a){function b(q){for(var u=0;u<q.length;u++)d[q[u]]=!0}var c=[],d=[];Hb=Ib(a);for(var e=0;e<qb.length;e++){var g=qb[e],h=Jb(g);if(h){for(var k=g.add||[],l=0;l<k.length;l++)c[k[l]]=!0;b(g.block||[])}else null===h&&b(g.block||[])}for(var m=[],n=0;n<sb.length;n++)c[n]&&!d[n]&&(m[n]=!0);return m},Jb=function(a){for(var b=a["if"]||[],c=0;c<b.length;c++){var d=Hb(b[c]);if(0===d)return!1;if(2===d)return null}for(var e=a.unless||[],g=0;g<e.length;g++){var h=Hb(e[g]);if(2===h)return null;
if(1===h)return!1}return!0},Ib=function(a){var b=[];return function(c){void 0===b[c]&&(b[c]=Fb(rb[c],a));return b[c]}};/*
 Copyright (c) 2014 Derek Brans, MIT license https://github.com/krux/postscribe/blob/master/LICENSE. Portions derived from simplehtmlparser, which is licensed under the Apache License, Version 2.0 */
var D=window,F=document,Zb=navigator,$b=F.currentScript&&F.currentScript.src,ac=function(a,b){var c=D[a];D[a]=void 0===c?b:c;return D[a]},bc=function(a,b){b&&(a.addEventListener?a.onload=b:a.onreadystatechange=function(){a.readyState in{loaded:1,complete:1}&&(a.onreadystatechange=null,b())})},cc=function(a,b,c){var d=F.createElement("script");d.type="text/javascript";d.async=!0;d.src=a;bc(d,b);c&&(d.onerror=c);var e;if(null===oa)b:{var g=la.document,h=g.querySelector&&g.querySelector("script[nonce]");
if(h){var k=h.nonce||h.getAttribute("nonce");if(k&&ma.test(k)){oa=k;break b}}oa=""}e=oa;e&&d.setAttribute("nonce",e);var l=F.getElementsByTagName("script")[0]||F.body||F.head;l.parentNode.insertBefore(d,l);return d},dc=function(){if($b){var a=$b.toLowerCase();if(0===a.indexOf("https://"))return 2;if(0===a.indexOf("http://"))return 3}return 1},ec=function(a,b){var c=F.createElement("iframe");c.height="0";c.width="0";c.style.display="none";c.style.visibility="hidden";var d=F.body&&F.body.lastChild||
F.body||F.head;d.parentNode.insertBefore(c,d);bc(c,b);void 0!==a&&(c.src=a);return c},fc=function(a,b,c){var d=new Image(1,1);d.onload=function(){d.onload=null;b&&b()};d.onerror=function(){d.onerror=null;c&&c()};d.src=a;return d},gc=function(a,b,c,d){a.addEventListener?a.addEventListener(b,c,!!d):a.attachEvent&&a.attachEvent("on"+b,c)},hc=function(a,b,c){a.removeEventListener?a.removeEventListener(b,c,!1):a.detachEvent&&a.detachEvent("on"+b,c)},G=function(a){D.setTimeout(a,0)},ic=function(a,b){return a&&
b&&a.attributes&&a.attributes[b]?a.attributes[b].value:null},jc=function(a){var b=a.innerText||a.textContent||"";b&&" "!=b&&(b=b.replace(/^[\s\xa0]+|[\s\xa0]+$/g,""));b&&(b=b.replace(/(\xa0+|\s{2,}|\n|\r\t)/g," "));return b},kc=function(a){var b=F.createElement("div");b.innerHTML="A<div>"+a+"</div>";b=b.lastChild;for(var c=[];b.firstChild;)c.push(b.removeChild(b.firstChild));return c},lc=function(a,b,c){c=c||100;for(var d={},e=0;e<b.length;e++)d[b[e]]=!0;for(var g=a,h=0;g&&h<=c;h++){if(d[String(g.tagName).toLowerCase()])return g;
g=g.parentElement}return null},mc=function(a,b){var c=a[b];c&&"string"===typeof c.animVal&&(c=c.animVal);return c};var oc=function(a){return nc?F.querySelectorAll(a):null},pc=function(a,b){if(!nc)return null;if(Element.prototype.closest)try{return a.closest(b)}catch(e){return null}var c=Element.prototype.matches||Element.prototype.webkitMatchesSelector||Element.prototype.mozMatchesSelector||Element.prototype.msMatchesSelector||Element.prototype.oMatchesSelector,d=a;if(!F.documentElement.contains(d))return null;do{try{if(c.call(d,b))return d}catch(e){break}d=d.parentElement||d.parentNode}while(null!==d&&1===d.nodeType);
return null},qc=!1;if(F.querySelectorAll)try{var sc=F.querySelectorAll(":root");sc&&1==sc.length&&sc[0]==F.documentElement&&(qc=!0)}catch(a){}var nc=qc;var H={la:"_ee",gc:"event_callback",Ra:"event_timeout",w:"gtag.config",O:"allow_ad_personalization_signals",oc:"restricted_data_processing",U:"cookie_expires",Qa:"cookie_update",Ba:"session_duration",V:"user_properties"};var Ic=/[A-Z]+/,Jc=/\s/,Kc=function(a){if(f(a)&&(a=Ea(a),!Jc.test(a))){var b=a.indexOf("-");if(!(0>b)){var c=a.substring(0,b);if(Ic.test(c)){for(var d=a.substring(b+1).split("/"),e=0;e<d.length;e++)if(!d[e])return;return{id:a,prefix:c,containerId:c+"-"+d[0],h:d}}}}},Mc=function(a){for(var b={},c=0;c<a.length;++c){var d=Kc(a[c]);d&&(b[d.id]=d)}Lc(b);var e=[];za(b,function(g,h){e.push(h)});return e};
function Lc(a){var b=[],c;for(c in a)if(a.hasOwnProperty(c)){var d=a[c];"AW"===d.prefix&&d.h[1]&&b.push(d.containerId)}for(var e=0;e<b.length;++e)delete a[b[e]]};var Nc={},Oc=null,Pc=Math.random();Nc.m="GTM-T7N7Z2Z";Nc.Db="c61";var Qc={__cl:!0,__ecl:!0,__ehl:!0,__evl:!0,__fal:!0,__fil:!0,__fsl:!0,__hl:!0,__jel:!0,__lcl:!0,__sdl:!0,__tl:!0,__ytl:!0,__paused:!0,__tg:!0},Rc="www.googletagmanager.com/gtm.js";var Sc=Rc,Tc=null,Uc=null,Vc=null,Wc="//www.googletagmanager.com/a?id="+Nc.m+"&cv=141",Xc={},Yc={},Zc=function(){var a=Oc.sequence||0;Oc.sequence=a+1;return a};var $c={},I=function(a,b){$c[a]=$c[a]||[];$c[a][b]=!0},ad=function(a){for(var b=[],c=$c[a]||[],d=0;d<c.length;d++)c[d]&&(b[Math.floor(d/6)]^=1<<d%6);for(var e=0;e<b.length;e++)b[e]="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_".charAt(b[e]||0);return b.join("")};
var cd=function(){return"&tc="+sb.filter(function(a){return a}).length},fd=function(){dd||(dd=D.setTimeout(ed,500))},ed=function(){dd&&(D.clearTimeout(dd),dd=void 0);void 0===gd||hd[gd]&&!id&&!jd||(kd[gd]||ld.Fg()||0>=md--?(I("GTM",1),kd[gd]=!0):(ld.Zg(),fc(nd()),hd[gd]=!0,od=jd=id=""))},nd=function(){var a=gd;if(void 0===a)return"";var b=ad("GTM"),c=ad("TAGGING");return[pd,hd[a]?"":"&es=1",qd[a],b?"&u="+b:"",c?"&ut="+c:"",cd(),id,jd,od,"&z=0"].join("")},rd=function(){return[Wc,"&v=3&t=t","&pid="+
wa(),"&rv="+Nc.Db].join("")},sd="0.005000">Math.random(),pd=rd(),td=function(){pd=rd()},hd={},id="",jd="",od="",gd=void 0,qd={},kd={},dd=void 0,ld=function(a,b){var c=0,d=0;return{Fg:function(){if(c<a)return!1;Fa()-d>=b&&(c=0);return c>=a},Zg:function(){Fa()-d>=b&&(c=0);c++;d=Fa()}}}(2,1E3),md=1E3,ud=function(a,b){if(sd&&!kd[a]&&gd!==a){ed();gd=a;id="";var c;c=0===b.indexOf("gtm.")?encodeURIComponent(b):"*";qd[a]="&e="+c+"&eid="+a;fd()}},vd=function(a,b,c){if(sd&&!kd[a]&&b){a!==gd&&
(ed(),gd=a);var d=String(b[Gb.ma]||"").replace(/_/g,"");0===d.indexOf("cvt")&&(d="cvt");var e=c+d;id=id?id+"."+e:"&tr="+e;fd();2022<=nd().length&&ed()}},wd=function(a,b,c){if(sd&&!kd[a]){a!==gd&&(ed(),gd=a);var d=c+b;jd=jd?jd+"."+d:"&epr="+d;fd();2022<=nd().length&&ed()}};var xd={},yd=new xa,zd={},Ad={},Dd={name:"dataLayer",set:function(a,b){C(La(a,b),zd);Bd()},get:function(a){return Cd(a,2)},reset:function(){yd=new xa;zd={};Bd()}},Cd=function(a,b){if(2!=b){var c=yd.get(a);if(sd){var d=Ed(a);c!==d&&I("GTM",5)}return c}return Ed(a)},Ed=function(a,b,c){var d=a.split("."),e=!1,g=void 0;return e?g:Gd(d)},Gd=function(a){for(var b=zd,c=0;c<a.length;c++){if(null===b)return!1;if(void 0===b)break;b=b[a[c]]}return b};
var Id=function(a,b){Ad.hasOwnProperty(a)||(yd.set(a,b),C(La(a,b),zd),Bd())},Bd=function(a){za(Ad,function(b,c){yd.set(b,c);C(La(b,void 0),zd);C(La(b,c),zd);a&&delete Ad[b]})},Jd=function(a,b,c){xd[a]=xd[a]||{};var d=1!==c?Ed(b):yd.get(b);"array"===Na(d)||"object"===Na(d)?xd[a][b]=C(d):xd[a][b]=d},Kd=function(a,b){if(xd[a])return xd[a][b]};var Ld=function(){var a=!1;return a};var P=function(a,b,c,d){return(2===Md()||d||"http:"!=D.location.protocol?a:b)+c},Md=function(){var a=dc(),b;if(1===a)a:{var c=Sc;c=c.toLowerCase();for(var d="https://"+c,e="http://"+c,g=1,h=F.getElementsByTagName("script"),k=0;k<h.length&&100>k;k++){var l=h[k].src;if(l){l=l.toLowerCase();if(0===l.indexOf(e)){b=3;break a}1===g&&0===l.indexOf(d)&&(g=2)}}b=g}else b=a;return b};var ae=new RegExp(/^(.*\.)?(google|youtube|blogger|withgoogle)(\.com?)?(\.[a-z]{2})?\.?$/),be={cl:["ecl"],customPixels:["nonGooglePixels"],ecl:["cl"],ehl:["hl"],hl:["ehl"],html:["customScripts","customPixels","nonGooglePixels","nonGoogleScripts","nonGoogleIframes"],customScripts:["html","customPixels","nonGooglePixels","nonGoogleScripts","nonGoogleIframes"],nonGooglePixels:[],nonGoogleScripts:["nonGooglePixels"],nonGoogleIframes:["nonGooglePixels"]},ce={cl:["ecl"],customPixels:["customScripts","html"],
ecl:["cl"],ehl:["hl"],hl:["ehl"],html:["customScripts"],customScripts:["html"],nonGooglePixels:["customPixels","customScripts","html","nonGoogleScripts","nonGoogleIframes"],nonGoogleScripts:["customScripts","html"],nonGoogleIframes:["customScripts","html","nonGoogleScripts"]},de="google customPixels customScripts html nonGooglePixels nonGoogleScripts nonGoogleIframes".split(" ");
var fe=function(a){Yc.pntr=Yc.pntr||["nonGoogleScripts"];Yc.snppx=Yc.snppx||["nonGoogleScripts"];Yc.qpx=Yc.qpx||["nonGooglePixels"];var b=Cd("gtm.whitelist");b&&I("GTM",9);
var c=b&&Ka(Da(b),be),d=Cd("gtm.blacklist");d||(d=Cd("tagTypeBlacklist"))&&I("GTM",3);d?I("GTM",8):d=[];ee()&&(d=Da(d),d.push("nonGooglePixels","nonGoogleScripts","sandboxedScripts"));0<=t(Da(d),"google")&&I("GTM",2);var e=d&&Ka(Da(d),ce),g={};return function(h){var k=h&&h[Gb.ma];if(!k||"string"!=typeof k)return!0;k=k.replace(/^_*/,"");if(void 0!==g[k])return g[k];
var l=Yc[k]||[],m=a(k,l);if(b){var n;if(n=m)a:{if(0>t(c,k))if(l&&0<l.length)for(var q=0;q<l.length;q++){if(0>t(c,l[q])){I("GTM",11);n=!1;break a}}else{n=!1;break a}n=!0}m=n}var u=!1;if(d){var p=0<=t(e,k);if(p)u=p;else{var r=ya(e,l||[]);r&&I("GTM",10);u=r}}var v=!m||u;v||!(0<=t(l,"sandboxedScripts"))||c&&-1!==t(c,"sandboxedScripts")||(v=ya(e,de));return g[k]=v}},ee=function(){return ae.test(D.location&&D.location.hostname)};var ge={Uf:function(a,b){b[Gb.ld]&&"string"===typeof a&&(a=1==b[Gb.ld]?a.toLowerCase():a.toUpperCase());b.hasOwnProperty(Gb.nd)&&null===a&&(a=b[Gb.nd]);b.hasOwnProperty(Gb.pd)&&void 0===a&&(a=b[Gb.pd]);b.hasOwnProperty(Gb.od)&&!0===a&&(a=b[Gb.od]);b.hasOwnProperty(Gb.md)&&!1===a&&(a=b[Gb.md]);return a}};var he={active:!0,isWhitelisted:function(){return!0}},ie=function(a){var b=Oc.zones;!b&&a&&(b=Oc.zones=a());return b};var je=function(){};var ke=!1,le=0,me=[];function ne(a){if(!ke){var b=F.createEventObject,c="complete"==F.readyState,d="interactive"==F.readyState;if(!a||"readystatechange"!=a.type||c||!b&&d){ke=!0;for(var e=0;e<me.length;e++)G(me[e])}me.push=function(){for(var g=0;g<arguments.length;g++)G(arguments[g]);return 0}}}function oe(){if(!ke&&140>le){le++;try{F.documentElement.doScroll("left"),ne()}catch(a){D.setTimeout(oe,50)}}}var pe=function(a){ke?a():me.push(a)};var qe={},re={},se=function(a,b,c,d){if(!re[a]||Qc[b]||"__zone"===b)return-1;var e={};Pa(d)&&(e=C(d,e));e.id=c;e.status="timeout";return re[a].tags.push(e)-1},te=function(a,b,c,d){if(re[a]){var e=re[a].tags[b];e&&(e.status=c,e.executionTime=d)}};function ue(a){for(var b=qe[a]||[],c=0;c<b.length;c++)b[c]();qe[a]={push:function(d){d(Nc.m,re[a])}}}
var xe=function(a,b,c){re[a]={tags:[]};qa(b)&&ve(a,b);c&&D.setTimeout(function(){return ue(a)},Number(c));return we(a)},ve=function(a,b){qe[a]=qe[a]||[];qe[a].push(Ha(function(){return G(function(){b(Nc.m,re[a])})}))};function we(a){var b=0,c=0,d=!1;return{add:function(){c++;return Ha(function(){b++;d&&b>=c&&ue(a)})},Ef:function(){d=!0;b>=c&&ue(a)}}};var ye=function(){function a(d){return!ra(d)||0>d?0:d}if(!Oc._li&&D.performance&&D.performance.timing){var b=D.performance.timing.navigationStart,c=ra(Dd.get("gtm.start"))?Dd.get("gtm.start"):0;Oc._li={cst:a(c-b),cbt:a(Uc-b)}}};var Ce=!1,De=function(){return D.GoogleAnalyticsObject&&D[D.GoogleAnalyticsObject]},Ee=!1;
var Fe=function(a){D.GoogleAnalyticsObject||(D.GoogleAnalyticsObject=a||"ga");var b=D.GoogleAnalyticsObject;if(D[b])D.hasOwnProperty(b)||I("GTM",12);else{var c=function(){c.q=c.q||[];c.q.push(arguments)};c.l=Number(new Date);D[b]=c}ye();return D[b]},Ge=function(a,b,c,d){b=String(b).replace(/\s+/g,"").split(",");var e=De();e(a+"require","linker");e(a+"linker:autoLink",b,c,d)};
var Ie=function(){},He=function(){return D.GoogleAnalyticsObject||"ga"};/*

 Copyright The Closure Library Authors.
 SPDX-License-Identifier: Apache-2.0
*/
var Ke=/^(?:(?:https?|mailto|ftp):|[^:/?#]*(?:[/?#]|$))/i;var Le=/:[0-9]+$/,Me=function(a,b,c){for(var d=a.split("&"),e=0;e<d.length;e++){var g=d[e].split("=");if(decodeURIComponent(g[0]).replace(/\+/g," ")===b){var h=g.slice(1).join("=");return c?h:decodeURIComponent(h).replace(/\+/g," ")}}},Pe=function(a,b,c,d,e){b&&(b=String(b).toLowerCase());if("protocol"===b||"port"===b)a.protocol=Ne(a.protocol)||Ne(D.location.protocol);"port"===b?a.port=String(Number(a.hostname?a.port:D.location.port)||("http"==a.protocol?80:"https"==a.protocol?443:"")):"host"===b&&
(a.hostname=(a.hostname||D.location.hostname).replace(Le,"").toLowerCase());var g=b,h,k=Ne(a.protocol);g&&(g=String(g).toLowerCase());switch(g){case "url_no_fragment":h=Oe(a);break;case "protocol":h=k;break;case "host":h=a.hostname.replace(Le,"").toLowerCase();if(c){var l=/^www\d*\./.exec(h);l&&l[0]&&(h=h.substr(l[0].length))}break;case "port":h=String(Number(a.port)||("http"==k?80:"https"==k?443:""));break;case "path":a.pathname||a.hostname||I("TAGGING",1);h="/"==a.pathname.substr(0,1)?a.pathname:
"/"+a.pathname;var m=h.split("/");0<=t(d||[],m[m.length-1])&&(m[m.length-1]="");h=m.join("/");break;case "query":h=a.search.replace("?","");e&&(h=Me(h,e,void 0));break;case "extension":var n=a.pathname.split(".");h=1<n.length?n[n.length-1]:"";h=h.split("/")[0];break;case "fragment":h=a.hash.replace("#","");break;default:h=a&&a.href}return h},Ne=function(a){return a?a.replace(":","").toLowerCase():""},Oe=function(a){var b="";if(a&&a.href){var c=a.href.indexOf("#");b=0>c?a.href:a.href.substr(0,c)}return b},
Qe=function(a){var b=F.createElement("a");a&&(b.href=a);var c=b.pathname;"/"!==c[0]&&(a||I("TAGGING",1),c="/"+c);var d=b.hostname.replace(Le,"");return{href:b.href,protocol:b.protocol,host:b.host,hostname:d,pathname:c,search:b.search,hash:b.hash,port:b.port}};function Ve(a,b,c,d){var e=sb[a],g=We(a,b,c,d);if(!g)return null;var h=Bb(e[Gb.Kd],c,[]);if(h&&h.length){var k=h[0];g=Ve(k.index,{s:g,o:1===k.Wd?b.terminate:g,terminate:b.terminate},c,d)}return g}
function We(a,b,c,d){function e(){if(g[Gb.jf])k();else{var w=Cb(g,c,[]),y=se(c.id,String(g[Gb.ma]),Number(g[Gb.Ld]),w[Gb.kf]),x=!1;w.vtp_gtmOnSuccess=function(){if(!x){x=!0;var A=Fa()-B;vd(c.id,sb[a],"5");te(c.id,y,"success",A);h()}};w.vtp_gtmOnFailure=function(){if(!x){x=!0;var A=Fa()-B;vd(c.id,sb[a],"6");te(c.id,y,"failure",A);k()}};w.vtp_gtmTagId=g.tag_id;
w.vtp_gtmEventId=c.id;vd(c.id,g,"1");var z=function(){var A=Fa()-B;vd(c.id,g,"7");te(c.id,y,"exception",A);x||(x=!0,k())};var B=Fa();try{Ab(w,c)}catch(A){z(A)}}}var g=sb[a],h=b.s,k=b.o,l=b.terminate;if(c.Jc(g))return null;var m=Bb(g[Gb.Md],c,[]);if(m&&m.length){var n=m[0],q=Ve(n.index,{s:h,o:k,terminate:l},c,d);if(!q)return null;h=q;k=2===n.Wd?l:q}if(g[Gb.Jd]||g[Gb.lf]){var u=g[Gb.Jd]?ub:c.ih,p=h,r=k;if(!u[a]){e=Ha(e);var v=Xe(a,u,e);h=v.s;k=v.o}return function(){u[a](p,r)}}return e}
function Xe(a,b,c){var d=[],e=[];b[a]=Ye(d,e,c);return{s:function(){b[a]=Ze;for(var g=0;g<d.length;g++)d[g]()},o:function(){b[a]=$e;for(var g=0;g<e.length;g++)e[g]()}}}function Ye(a,b,c){return function(d,e){a.push(d);b.push(e);c()}}function Ze(a){a()}function $e(a,b){b()};var cf=function(a,b){for(var c=[],d=0;d<sb.length;d++)if(a.kb[d]){var e=sb[d];var g=b.add();try{var h=Ve(d,{s:g,o:g,terminate:g},a,d);h?c.push({ze:d,oe:Db(e),eg:h}):(af(d,a),g())}catch(l){g()}}b.Ef();c.sort(bf);for(var k=0;k<c.length;k++)c[k].eg();return 0<c.length};function bf(a,b){var c,d=b.oe,e=a.oe;c=d>e?1:d<e?-1:0;var g;if(0!==c)g=c;else{var h=a.ze,k=b.ze;g=h>k?1:h<k?-1:0}return g}
function af(a,b){if(!sd)return;var c=function(d){var e=b.Jc(sb[d])?"3":"4",g=Bb(sb[d][Gb.Kd],b,[]);g&&g.length&&c(g[0].index);vd(b.id,sb[d],e);var h=Bb(sb[d][Gb.Md],b,[]);h&&h.length&&c(h[0].index)};c(a);}
var df=!1,ef=function(a,b,c,d,e){if("gtm.js"==b){if(df)return!1;df=!0}ud(a,b);var g=xe(a,d,e);Jd(a,"event",1);Jd(a,"ecommerce",1);Jd(a,"gtm");var h={id:a,name:b,Jc:fe(c),kb:[],ih:[],he:function(){I("GTM",6)}};h.kb=Kb(h);var k=cf(h,g);
if(!k)return k;for(var l=0;l<h.kb.length;l++)if(h.kb[l]){var m=sb[l];if(m&&!Qc[String(m[Gb.ma])])return!0}return!1};var gf=/^https?:\/\/www\.googletagmanager\.com/;function hf(){var a;return a}function kf(a,b){}
function jf(a){0!==a.indexOf("http://")&&0!==a.indexOf("https://")&&(a="https://"+a);"/"===a[a.length-1]&&(a=a.substring(0,a.length-1));return a}function lf(){var a=!1;return a};var mf=function(){this.eventModel={};this.targetConfig={};this.containerConfig={};this.Ma={};this.globalConfig={};this.s=function(){};this.o=function(){}},nf=function(a){var b=new mf;b.eventModel=a;return b},of=function(a,b){a.targetConfig=b;return a},pf=function(a,b){a.containerConfig=b;return a},qf=function(a,b){a.Ma=b;return a},rf=function(a,b){a.globalConfig=b;return a},sf=function(a,b){a.s=b;return a},tf=function(a,b){a.o=b;return a};
mf.prototype.getWithConfig=function(a){if(void 0!==this.eventModel[a])return this.eventModel[a];if(void 0!==this.targetConfig[a])return this.targetConfig[a];if(void 0!==this.containerConfig[a])return this.containerConfig[a];if(void 0!==this.Ma[a])return this.Ma[a];if(void 0!==this.globalConfig[a])return this.globalConfig[a]};
var uf=function(a){function b(e){za(e,function(g){c[g]=null})}var c={};b(a.eventModel);b(a.targetConfig);b(a.containerConfig);b(a.globalConfig);var d=[];za(c,function(e){d.push(e)});return d};var vf={},wf=["G"];vf.Be="";var xf=vf.Be.split(",");function yf(){var a=Oc;return a.gcq=a.gcq||new zf}
var Af=function(a,b,c){yf().register(a,b,c)},Bf=function(a,b,c,d){yf().push("event",[b,a],c,d)},Cf=function(a,b){yf().push("config",[a],b)},Df={},Ef=function(){this.status=1;this.containerConfig={};this.targetConfig={};this.Ma={};this.pe=null;this.ae=!1},Ff=function(a,b,c,d,e){this.type=a;this.Ae=b;this.L=c||"";this.Hb=d;this.defer=e},zf=function(){this.Sd={};this.$d={};this.eb=[]},Gf=function(a,b){var c=Kc(b);return a.Sd[c.containerId]=a.Sd[c.containerId]||new Ef},Hf=function(a,b,c,d){if(d.L){var e=
Gf(a,d.L),g=e.pe;if(g){var h=C(c),k=C(e.targetConfig[d.L]),l=C(e.containerConfig),m=C(e.Ma),n=C(a.$d),q=Cd("gtm.uniqueEventId"),u=Kc(d.L).prefix,p=tf(sf(rf(qf(pf(of(nf(h),k),l),m),n),function(){wd(q,u,"2");}),function(){wd(q,u,"3");});try{wd(q,u,"1");3===g.length?g(b,d.Ae,p):4===g.length&&
g(d.L,b,d.Ae,p)}catch(r){wd(q,u,"4");}}}};
zf.prototype.register=function(a,b,c){if(3!==Gf(this,a).status){Gf(this,a).pe=b;Gf(this,a).status=3;c&&(Gf(this,a).Ma=c);var d=Kc(a),e=Df[d.containerId];if(void 0!==e){var g=Oc[d.containerId].bootstrap,h=d.prefix.toUpperCase();Oc[d.containerId]._spx&&(h=h.toLowerCase());var k=Cd("gtm.uniqueEventId"),l=h,m=Fa()-g;if(sd&&!kd[k]){k!==gd&&(ed(),gd=k);var n=l+"."+Math.floor(g-e)+"."+Math.floor(m);od=od?od+","+n:"&cl="+n}delete Df[d.containerId]}this.flush()}};
zf.prototype.push=function(a,b,c,d){var e=Math.floor(Fa()/1E3);if(c){var g=Kc(c),h;if(h=g){var k;if(k=1===Gf(this,c).status)a:{var l=g.prefix;k=!0}h=k}if(h&&(Gf(this,c).status=2,this.push("require",[],g.containerId),Df[g.containerId]=Fa(),!Ld())){var m=encodeURIComponent(g.containerId),n=("http:"!=D.location.protocol?"https:":"http:")+
"//www.googletagmanager.com";cc(n+"/gtag/js?id="+m+"&l=dataLayer&cx=c")}}this.eb.push(new Ff(a,e,c,b,d));d||this.flush()};
zf.prototype.flush=function(a){for(var b=this;this.eb.length;){var c=this.eb[0];if(c.defer)c.defer=!1,this.eb.push(c);else switch(c.type){case "require":if(3!==Gf(this,c.L).status&&!a)return;break;case "set":za(c.Hb[0],function(l,m){C(La(l,m),b.$d)});break;case "config":var d=c.Hb[0],e=!!d[H.yb];delete d[H.yb];var g=Gf(this,c.L),h=Kc(c.L),k=h.containerId===h.id;e||(k?g.containerConfig={}:g.targetConfig[c.L]={});g.ae&&e||Hf(this,H.w,d,c);g.ae=!0;delete d[H.la];k?C(d,g.containerConfig):C(d,g.targetConfig[c.L]);
break;case "event":Hf(this,c.Hb[1],c.Hb[0],c)}this.eb.shift()}};var If=function(a,b,c){for(var d=[],e=String(b||document.cookie).split(";"),g=0;g<e.length;g++){var h=e[g].split("="),k=h[0].replace(/^\s*|\s*$/g,"");if(k&&k==a){var l=h.slice(1).join("=").replace(/^\s*|\s*$/g,"");l&&c&&(l=decodeURIComponent(l));d.push(l)}}return d},Lf=function(a,b,c,d){var e=Jf(a,d);if(1===e.length)return e[0].id;if(0!==e.length){e=Kf(e,function(g){return g.Mb},b);if(1===e.length)return e[0].id;e=Kf(e,function(g){return g.lb},c);return e[0]?e[0].id:void 0}};
function Mf(a,b,c){var d=document.cookie;document.cookie=a;var e=document.cookie;return d!=e||void 0!=c&&0<=If(b,e).indexOf(c)}
var Qf=function(a,b,c,d,e,g){d=d||"auto";var h={path:c||"/"};e&&(h.expires=e);"none"!==d&&(h.domain=d);var k;a:{var l=b,m;if(void 0==l)m=a+"=deleted; expires="+(new Date(0)).toUTCString();else{g&&(l=encodeURIComponent(l));var n=l;n&&1200<n.length&&(n=n.substring(0,1200));l=n;m=a+"="+l}var q=void 0,u=void 0,p;for(p in h)if(h.hasOwnProperty(p)){var r=h[p];if(null!=r)switch(p){case "secure":r&&(m+="; secure");break;case "domain":q=r;break;default:"path"==p&&(u=r),"expires"==p&&r instanceof Date&&(r=
r.toUTCString()),m+="; "+p+"="+r}}if("auto"===q){for(var v=Nf(),w=0;w<v.length;++w){var y="none"!=v[w]?v[w]:void 0;if(!Pf(y,u)&&Mf(m+(y?"; domain="+y:""),a,l)){k=!0;break a}}k=!1}else q&&"none"!=q&&(m+="; domain="+q),k=!Pf(q,u)&&Mf(m,a,l)}return k};function Kf(a,b,c){for(var d=[],e=[],g,h=0;h<a.length;h++){var k=a[h],l=b(k);l===c?d.push(k):void 0===g||l<g?(e=[k],g=l):l===g&&e.push(k)}return 0<d.length?d:e}
function Jf(a,b){for(var c=[],d=If(a),e=0;e<d.length;e++){var g=d[e].split("."),h=g.shift();if(!b||-1!==b.indexOf(h)){var k=g.shift();k&&(k=k.split("-"),c.push({id:g.join("."),Mb:1*k[0]||1,lb:1*k[1]||1}))}}return c}
var Rf=/^(www\.)?google(\.com?)?(\.[a-z]{2})?$/,Sf=/(^|\.)doubleclick\.net$/i,Pf=function(a,b){return Sf.test(document.location.hostname)||"/"===b&&Rf.test(a)},Nf=function(){var a=[],b=document.location.hostname.split(".");if(4===b.length){var c=b[b.length-1];if(parseInt(c,10).toString()===c)return["none"]}for(var d=b.length-2;0<=d;d--)a.push(b.slice(d).join("."));var e=document.location.hostname;Sf.test(e)||Rf.test(e)||a.push("none");return a};var Tf="".split(/,/),Uf=!1;var Vf=null,Wf={},Xf={},Yf;function Zf(a,b){var c={event:a};b&&(c.eventModel=C(b),b[H.gc]&&(c.eventCallback=b[H.gc]),b[H.Ra]&&(c.eventTimeout=b[H.Ra]));return c}
var eg={config:function(a){},
event:function(a){var b=a[1];if(f(b)&&!(3<a.length)){var c;if(2<a.length){if(!Pa(a[2])&&void 0!=a[2])return;c=a[2]}var d=Zf(b,c);return d}},js:function(a){if(2==a.length&&a[1].getTime)return{event:"gtm.js","gtm.start":a[1].getTime()}},policy:function(a){3===a.length&&(void 0).Ih().Ah(a[1],a[2])},set:function(a){var b;2==a.length&&Pa(a[1])?b=
C(a[1]):3==a.length&&f(a[1])&&(b={},Pa(a[2])||ua(a[2])?b[a[1]]=C(a[2]):b[a[1]]=a[2]);if(b){b._clear=!0;return b}}},fg={policy:!0};var gg=function(a,b){var c=a.hide;if(c&&void 0!==c[b]&&c.end){c[b]=!1;var d=!0,e;for(e in c)if(c.hasOwnProperty(e)&&!0===c[e]){d=!1;break}d&&(c.end(),c.end=null)}},ig=function(a){var b=hg(),c=b&&b.hide;c&&c.end&&(c[a]=!0)};var pg=function(a){if(og(a))return a;this.ph=a};pg.prototype.lg=function(){return this.ph};var og=function(a){return!a||"object"!==Na(a)||Pa(a)?!1:"getUntrustedUpdateValue"in a};pg.prototype.getUntrustedUpdateValue=pg.prototype.lg;var qg=!1,rg=[];function sg(){if(!qg){qg=!0;for(var a=0;a<rg.length;a++)G(rg[a])}}var tg=function(a){qg?G(a):rg.push(a)};var ug=[],vg=!1,wg=function(a){return D["dataLayer"].push(a)},xg=function(a){var b=Oc["dataLayer"],c=b?b.subscribers:1,d=0;return function(){++d===c&&a()}};
function yg(a){var b=a._clear;za(a,function(g,h){"_clear"!==g&&(b&&Id(g,void 0),Id(g,h))});Tc||(Tc=a["gtm.start"]);var c=a.event;if(!c)return!1;var d=a["gtm.uniqueEventId"];d||(d=Zc(),a["gtm.uniqueEventId"]=d,Id("gtm.uniqueEventId",d));Vc=c;var e=zg(a);Vc=null;switch(c){case "gtm.init":I("GTM",19),e&&I("GTM",20)}return e}
function zg(a){var b=a.event,c=a["gtm.uniqueEventId"],d,e=Oc.zones;d=e?e.checkState(Nc.m,c):he;return d.active?ef(c,b,d.isWhitelisted,a.eventCallback,a.eventTimeout)?!0:!1:!1}
function Ag(){for(var a=!1;!vg&&0<ug.length;){vg=!0;delete zd.eventModel;Bd();var b=ug.shift();if(null!=b){var c=og(b);if(c){var d=b;b=og(d)?d.getUntrustedUpdateValue():void 0;for(var e=["gtm.whitelist","gtm.blacklist","tagTypeBlacklist"],g=0;g<e.length;g++){var h=e[g],k=Cd(h,1);if(ua(k)||Pa(k))k=C(k);Ad[h]=k}}try{if(qa(b))try{b.call(Dd)}catch(v){}else if(ua(b)){var l=b;if(f(l[0])){var m=
l[0].split("."),n=m.pop(),q=l.slice(1),u=Cd(m.join("."),2);if(void 0!==u&&null!==u)try{u[n].apply(u,q)}catch(v){}}}else{var p=b;if(p&&("[object Arguments]"==Object.prototype.toString.call(p)||Object.prototype.hasOwnProperty.call(p,"callee"))){a:{if(b.length&&f(b[0])){var r=eg[b[0]];if(r&&(!c||!fg[b[0]])){b=r(b);break a}}b=void 0}if(!b){vg=!1;continue}}a=yg(b)||a}}finally{c&&Bd(!0)}}vg=!1}
return!a}function Bg(){var a=Ag();try{gg(D["dataLayer"],Nc.m)}catch(b){}return a}
var Dg=function(){var a=ac("dataLayer",[]),b=ac("google_tag_manager",{});b=b["dataLayer"]=b["dataLayer"]||{};pe(function(){b.gtmDom||(b.gtmDom=!0,a.push({event:"gtm.dom"}))});tg(function(){b.gtmLoad||(b.gtmLoad=!0,a.push({event:"gtm.load"}))});b.subscribers=(b.subscribers||0)+1;var c=a.push;a.push=function(){var d;if(0<Oc.SANDBOXED_JS_SEMAPHORE){d=[];for(var e=0;e<arguments.length;e++)d[e]=new pg(arguments[e])}else d=[].slice.call(arguments,0);var g=c.apply(a,d);ug.push.apply(ug,d);if(300<
this.length)for(I("GTM",4);300<this.length;)this.shift();var h="boolean"!==typeof g||g;return Ag()&&h};ug.push.apply(ug,a.slice(0));Cg()&&G(Bg)},Cg=function(){var a=!0;return a};var Eg={};Eg.zb=new String("undefined");
var Fg=function(a){this.resolve=function(b){for(var c=[],d=0;d<a.length;d++)c.push(a[d]===Eg.zb?b:a[d]);return c.join("")}};Fg.prototype.toString=function(){return this.resolve("undefined")};Fg.prototype.valueOf=Fg.prototype.toString;Eg.qf=Fg;Eg.wc={};Eg.Xf=function(a){return new Fg(a)};var Gg={};Eg.$g=function(a,b){var c=Zc();Gg[c]=[a,b];return c};Eg.Td=function(a){var b=a?0:1;return function(c){var d=Gg[c];if(d&&"function"===typeof d[b])d[b]();Gg[c]=void 0}};Eg.Dg=function(a){for(var b=!1,c=!1,
d=2;d<a.length;d++)b=b||8===a[d],c=c||16===a[d];return b&&c};Eg.Og=function(a){if(a===Eg.zb)return a;var b=Zc();Eg.wc[b]=a;return'google_tag_manager["'+Nc.m+'"].macro('+b+")"};Eg.Hg=function(a,b,c){a instanceof Eg.qf&&(a=a.resolve(Eg.$g(b,c)),b=pa);return{Hc:a,s:b}};var Hg=function(a,b,c){function d(g,h){var k=g[h];return k}var e={event:b,"gtm.element":a,"gtm.elementClasses":d(a,"className"),"gtm.elementId":a["for"]||ic(a,"id")||"","gtm.elementTarget":a.formTarget||d(a,"target")||""};c&&(e["gtm.triggers"]=c.join(","));e["gtm.elementUrl"]=(a.attributes&&a.attributes.formaction?a.formAction:"")||a.action||d(a,"href")||a.src||a.code||a.codebase||
"";return e},Ig=function(a){Oc.hasOwnProperty("autoEventsSettings")||(Oc.autoEventsSettings={});var b=Oc.autoEventsSettings;b.hasOwnProperty(a)||(b[a]={});return b[a]},Jg=function(a,b,c){Ig(a)[b]=c},Kg=function(a,b,c,d){var e=Ig(a),g=Ga(e,b,d);e[b]=c(g)},Lg=function(a,b,c){var d=Ig(a);return Ga(d,b,c)};var Mg=function(){for(var a=Zb.userAgent+(F.cookie||"")+(F.referrer||""),b=a.length,c=D.history.length;0<c;)a+=c--^b++;var d=1,e,g,h;if(a)for(d=0,g=a.length-1;0<=g;g--)h=a.charCodeAt(g),d=(d<<6&268435455)+h+(h<<14),e=d&266338304,d=0!=e?d^e>>21:d;return[Math.round(2147483647*Math.random())^d&2147483647,Math.round(Fa()/1E3)].join(".")},Pg=function(a,b,c,d){var e=Ng(b);return Lf(a,e,Og(c),d)},Qg=function(a,b,c,d){var e=""+Ng(c),g=Og(d);1<g&&(e+="-"+g);return[b,e,a].join(".")},Ng=function(a){if(!a)return 1;
a=0===a.indexOf(".")?a.substr(1):a;return a.split(".").length},Og=function(a){if(!a||"/"===a)return 1;"/"!==a[0]&&(a="/"+a);"/"!==a[a.length-1]&&(a+="/");return a.split("/").length-1};var Rg=["1"],Sg={},Wg=function(a,b,c,d){var e=Tg(a);Sg[e]||Ug(e,b,c)||(Vg(e,Mg(),b,c,d),Ug(e,b,c))};function Vg(a,b,c,d,e){var g=Qg(b,"1",d,c);Qf(a,g,c,d,0==e?void 0:new Date(Fa()+1E3*(void 0==e?7776E3:e)))}function Ug(a,b,c){var d=Pg(a,b,c,Rg);d&&(Sg[a]=d);return d}function Tg(a){return(a||"_gcl")+"_au"};var Xg=function(){for(var a=[],b=F.cookie.split(";"),c=/^\s*_gac_(UA-\d+-\d+)=\s*(.+?)\s*$/,d=0;d<b.length;d++){var e=b[d].match(c);e&&a.push({cd:e[1],value:e[2]})}var g={};if(!a||!a.length)return g;for(var h=0;h<a.length;h++){var k=a[h].value.split(".");"1"==k[0]&&3==k.length&&k[1]&&(g[a[h].cd]||(g[a[h].cd]=[]),g[a[h].cd].push({timestamp:k[1],ig:k[2]}))}return g};function Yg(){for(var a=Zg,b={},c=0;c<a.length;++c)b[a[c]]=c;return b}function $g(){var a="ABCDEFGHIJKLMNOPQRSTUVWXYZ";a+=a.toLowerCase()+"0123456789-_";return a+"."}var Zg,ah;function bh(a){Zg=Zg||$g();ah=ah||Yg();for(var b=[],c=0;c<a.length;c+=3){var d=c+1<a.length,e=c+2<a.length,g=a.charCodeAt(c),h=d?a.charCodeAt(c+1):0,k=e?a.charCodeAt(c+2):0,l=g>>2,m=(g&3)<<4|h>>4,n=(h&15)<<2|k>>6,q=k&63;e||(q=64,d||(n=64));b.push(Zg[l],Zg[m],Zg[n],Zg[q])}return b.join("")}
function ch(a){function b(l){for(;d<a.length;){var m=a.charAt(d++),n=ah[m];if(null!=n)return n;if(!/^[\s\xa0]*$/.test(m))throw Error("Unknown base64 encoding at char: "+m);}return l}Zg=Zg||$g();ah=ah||Yg();for(var c="",d=0;;){var e=b(-1),g=b(0),h=b(64),k=b(64);if(64===k&&-1===e)return c;c+=String.fromCharCode(e<<2|g>>4);64!=h&&(c+=String.fromCharCode(g<<4&240|h>>2),64!=k&&(c+=String.fromCharCode(h<<6&192|k)))}};var dh;function eh(a,b){if(!a||b===F.location.hostname)return!1;for(var c=0;c<a.length;c++)if(a[c]instanceof RegExp){if(a[c].test(b))return!0}else if(0<=b.indexOf(a[c]))return!0;return!1}
var ih=function(){var a=fh,b=gh,c=hh(),d=function(h){a(h.target||h.srcElement||{})},e=function(h){b(h.target||h.srcElement||{})};if(!c.init){gc(F,"mousedown",d);gc(F,"keyup",d);gc(F,"submit",e);var g=HTMLFormElement.prototype.submit;HTMLFormElement.prototype.submit=function(){b(this);g.call(this)};c.init=!0}},hh=function(){var a=ac("google_tag_data",{}),b=a.gl;b&&b.decorators||(b={decorators:[]},a.gl=b);return b};var jh=/(.*?)\*(.*?)\*(.*)/,kh=/^https?:\/\/([^\/]*?)\.?cdn\.ampproject\.org\/?(.*)/,lh=/^(?:www\.|m\.|amp\.)+/,mh=/([^?#]+)(\?[^#]*)?(#.*)?/,nh=/(.*?)(^|&)_gl=([^&]*)&?(.*)/,ph=function(a){var b=[],c;for(c in a)if(a.hasOwnProperty(c)){var d=a[c];void 0!==d&&d===d&&null!==d&&"[object Object]"!==d.toString()&&(b.push(c),b.push(bh(String(d))))}var e=b.join("*");return["1",oh(e),e].join("*")},oh=function(a,b){var c=[window.navigator.userAgent,(new Date).getTimezoneOffset(),window.navigator.userLanguage||
window.navigator.language,Math.floor((new Date).getTime()/60/1E3)-(void 0===b?0:b),a].join("*"),d;if(!(d=dh)){for(var e=Array(256),g=0;256>g;g++){for(var h=g,k=0;8>k;k++)h=h&1?h>>>1^3988292384:h>>>1;e[g]=h}d=e}dh=d;for(var l=4294967295,m=0;m<c.length;m++)l=l>>>8^dh[(l^c.charCodeAt(m))&255];return((l^-1)>>>0).toString(36)},rh=function(){return function(a){var b=Qe(D.location.href),c=b.search.replace("?",""),d=Me(c,"_gl",!0)||"";a.query=qh(d)||{};var e=Pe(b,"fragment").match(nh);a.fragment=qh(e&&e[3]||
"")||{}}},sh=function(){var a=rh(),b=hh();b.data||(b.data={query:{},fragment:{}},a(b.data));var c={},d=b.data;d&&(Ia(c,d.query),Ia(c,d.fragment));return c},qh=function(a){var b;b=void 0===b?3:b;try{if(a){var c;a:{for(var d=a,e=0;3>e;++e){var g=jh.exec(d);if(g){c=g;break a}d=decodeURIComponent(d)}c=void 0}var h=c;if(h&&"1"===h[1]){var k=h[3],l;a:{for(var m=h[2],n=0;n<b;++n)if(m===oh(k,n)){l=!0;break a}l=!1}if(l){for(var q={},u=k?k.split("*"):[],p=0;p<u.length;p+=2)q[u[p]]=ch(u[p+1]);return q}}}}catch(r){}};
function th(a,b,c){function d(m){var n=m,q=nh.exec(n),u=n;if(q){var p=q[2],r=q[4];u=q[1];r&&(u=u+p+r)}m=u;var v=m.charAt(m.length-1);m&&"&"!==v&&(m+="&");return m+l}c=void 0===c?!1:c;var e=mh.exec(b);if(!e)return"";var g=e[1],h=e[2]||"",k=e[3]||"",l="_gl="+a;c?k="#"+d(k.substring(1)):h="?"+d(h.substring(1));return""+g+h+k}
function uh(a,b,c){for(var d={},e={},g=hh().decorators,h=0;h<g.length;++h){var k=g[h];(!c||k.forms)&&eh(k.domains,b)&&(k.fragment?Ia(e,k.callback()):Ia(d,k.callback()))}if(Ja(d)){var l=ph(d);if(c){if(a&&a.action){var m=(a.method||"").toLowerCase();if("get"===m){for(var n=a.childNodes||[],q=!1,u=0;u<n.length;u++){var p=n[u];if("_gl"===p.name){p.setAttribute("value",l);q=!0;break}}if(!q){var r=F.createElement("input");r.setAttribute("type","hidden");r.setAttribute("name","_gl");r.setAttribute("value",
l);a.appendChild(r)}}else if("post"===m){var v=th(l,a.action);Ke.test(v)&&(a.action=v)}}}else vh(l,a,!1)}if(!c&&Ja(e)){var w=ph(e);vh(w,a,!0)}}function vh(a,b,c){if(b.href){var d=th(a,b.href,void 0===c?!1:c);Ke.test(d)&&(b.href=d)}}
var fh=function(a){try{var b;a:{for(var c=a,d=100;c&&0<d;){if(c.href&&c.nodeName.match(/^a(?:rea)?$/i)){b=c;break a}c=c.parentNode;d--}b=null}var e=b;if(e){var g=e.protocol;"http:"!==g&&"https:"!==g||uh(e,e.hostname,!1)}}catch(h){}},gh=function(a){try{if(a.action){var b=Pe(Qe(a.action),"host");uh(a,b,!0)}}catch(c){}},wh=function(a,b,c,d){ih();var e={callback:a,domains:b,fragment:"fragment"===c,forms:!!d};hh().decorators.push(e)},xh=function(){var a=F.location.hostname,b=kh.exec(F.referrer);if(!b)return!1;
var c=b[2],d=b[1],e="";if(c){var g=c.split("/"),h=g[1];e="s"===h?decodeURIComponent(g[2]):decodeURIComponent(h)}else if(d){if(0===d.indexOf("xn--"))return!1;e=d.replace(/-/g,".").replace(/\.\./g,"-")}var k=a.replace(lh,""),l=e.replace(lh,""),m;if(!(m=k===l)){var n="."+l;m=k.substring(k.length-n.length,k.length)===n}return m},yh=function(a,b){return!1===a?!1:a||b||xh()};var zh={};var Ah=/^\w+$/,Bh=/^[\w-]+$/,Ch=/^~?[\w-]+$/,Dh={aw:"_aw",dc:"_dc",gf:"_gf",ha:"_ha",gp:"_gp"};function Eh(a){return a&&"string"==typeof a&&a.match(Ah)?a:"_gcl"}var Gh=function(){var a=Qe(D.location.href),b=Pe(a,"query",!1,void 0,"gclid"),c=Pe(a,"query",!1,void 0,"gclsrc"),d=Pe(a,"query",!1,void 0,"dclid");if(!b||!c){var e=a.hash.replace("#","");b=b||Me(e,"gclid",void 0);c=c||Me(e,"gclsrc",void 0)}return Fh(b,c,d)};
function Fh(a,b,c){var d={},e=function(g,h){d[h]||(d[h]=[]);d[h].push(g)};if(void 0!==a&&a.match(Bh))switch(b){case void 0:e(a,"aw");break;case "aw.ds":e(a,"aw");e(a,"dc");break;case "ds":e(a,"dc");break;case "3p.ds":(void 0==zh.gtm_3pds?0:zh.gtm_3pds)&&e(a,"dc");break;case "gf":e(a,"gf");break;case "ha":e(a,"ha");break;case "gp":e(a,"gp")}c&&e(c,"dc");return d}var Ih=function(a){var b=Gh();Hh(b,a)};
function Hh(a,b,c){function d(q,u){var p=Jh(q,e);p&&Qf(p,u,h,g,l,!0)}b=b||{};var e=Eh(b.prefix),g=b.domain||"auto",h=b.path||"/",k=void 0==b.Ja?7776E3:b.Ja;c=c||Fa();var l=0==k?void 0:new Date(c+1E3*k),m=Math.round(c/1E3),n=function(q){return["GCL",m,q].join(".")};a.aw&&(!0===b.Yh?d("aw",n("~"+a.aw[0])):d("aw",n(a.aw[0])));a.dc&&d("dc",n(a.dc[0]));a.gf&&d("gf",n(a.gf[0]));a.ha&&d("ha",n(a.ha[0]));a.gp&&d("gp",n(a.gp[0]))}
var Lh=function(a,b,c,d,e){for(var g=sh(),h=Eh(b),k=0;k<a.length;++k){var l=a[k];if(void 0!==Dh[l]){var m=Jh(l,h),n=g[m];if(n){var q=Math.min(Kh(n),Fa()),u;b:{for(var p=q,r=If(m,F.cookie),v=0;v<r.length;++v)if(Kh(r[v])>p){u=!0;break b}u=!1}u||Qf(m,n,c,d,0==e?void 0:new Date(q+1E3*(null==e?7776E3:e)),!0)}}}var w={prefix:b,path:c,domain:d};Hh(Fh(g.gclid,g.gclsrc),w)},Jh=function(a,b){var c=Dh[a];if(void 0!==c)return b+c},Kh=function(a){var b=a.split(".");return 3!==b.length||"GCL"!==b[0]?0:1E3*(Number(b[1])||
0)};function Mh(a){var b=a.split(".");if(3==b.length&&"GCL"==b[0]&&b[1])return b[2]}
var Nh=function(a,b,c,d,e){if(ua(b)){var g=Eh(e);wh(function(){for(var h={},k=0;k<a.length;++k){var l=Jh(a[k],g);if(l){var m=If(l,F.cookie);m.length&&(h[l]=m.sort()[m.length-1])}}return h},b,c,d)}},Oh=function(a){return a.filter(function(b){return Ch.test(b)})},Ph=function(a,b){for(var c=Eh(b&&b.prefix),d={},e=0;e<a.length;e++)Dh[a[e]]&&(d[a[e]]=Dh[a[e]]);za(d,function(g,h){var k=If(c+h,F.cookie);if(k.length){var l=k[0],m=Kh(l),n={};n[g]=[Mh(l)];Hh(n,b,m)}})};var Qh=/^\d+\.fls\.doubleclick\.net$/;function Rh(a){var b=Qe(D.location.href),c=Pe(b,"host",!1);if(c&&c.match(Qh)){var d=Pe(b,"path").split(a+"=");if(1<d.length)return d[1].split(";")[0].split("?")[0]}}
function Sh(a,b){if("aw"==a||"dc"==a){var c=Rh("gcl"+a);if(c)return c.split(".")}var d=Eh(b);if("_gcl"==d){var e;e=Gh()[a]||[];if(0<e.length)return e}var g=Jh(a,d),h;if(g){var k=[];if(F.cookie){var l=If(g,F.cookie);if(l&&0!=l.length){for(var m=0;m<l.length;m++){var n=Mh(l[m]);n&&-1===t(k,n)&&k.push(n)}h=Oh(k)}else h=k}else h=k}else h=[];return h}
var Th=function(){var a=Rh("gac");if(a)return decodeURIComponent(a);var b=Xg(),c=[];za(b,function(d,e){for(var g=[],h=0;h<e.length;h++)g.push(e[h].ig);g=Oh(g);g.length&&c.push(d+":"+g.join(","))});return c.join(";")},Uh=function(a,b,c,d,e){Wg(b,c,d,e);var g=Sg[Tg(b)],h=Gh().dc||[],k=!1;if(g&&0<h.length){var l=Oc.joined_au=Oc.joined_au||{},m=b||"_gcl";if(!l[m])for(var n=0;n<h.length;n++){var q="https://adservice.google.com/ddm/regclk",u=q=q+"?gclid="+h[n]+"&auiddc="+g;Zb.sendBeacon&&Zb.sendBeacon(u)||fc(u);k=l[m]=
!0}}null==a&&(a=k);if(a&&g){var p=Tg(b),r=Sg[p];r&&Vg(p,r,c,d,e)}};var Vh;if(3===Nc.Db.length)Vh="g";else{var Wh="G";Vh=Wh}
var Xh={"":"n",UA:"u",AW:"a",DC:"d",G:"e",GF:"f",HA:"h",GTM:Vh,OPT:"o"},Yh=function(a){var b=Nc.m.split("-"),c=b[0].toUpperCase(),d=Xh[c]||"i",e=a&&"GTM"===c?b[1]:"OPT"===c?b[1]:"",g;if(3===Nc.Db.length){var h=void 0;g="2"+(h||"w")}else g=
"";return g+d+Nc.Db+e};var ci=["input","select","textarea"],di=["button","hidden","image","reset","submit"],ei=function(a){var b=a.tagName.toLowerCase();return!va(ci,function(c){return c===b})||"input"===b&&va(di,function(c){return c===a.type.toLowerCase()})?!1:!0},fi=function(a){return a.form?a.form.tagName?a.form:F.getElementById(a.form):lc(a,["form"],100)},gi=function(a,b,c){if(!a.elements)return 0;for(var d=b.getAttribute(c),e=0,g=1;e<a.elements.length;e++){var h=a.elements[e];if(ei(h)){if(h.getAttribute(c)===d)return g;
g++}}return 0};var ji=!!D.MutationObserver,ki=void 0,li=function(a){if(!ki){var b=function(){var c=F.body;if(c)if(ji)(new MutationObserver(function(){for(var e=0;e<ki.length;e++)G(ki[e])})).observe(c,{childList:!0,subtree:!0});else{var d=!1;gc(c,"DOMNodeInserted",function(){d||(d=!0,G(function(){d=!1;for(var e=0;e<ki.length;e++)G(ki[e])}))})}};ki=[];F.body?b():G(b)}ki.push(a)};
var wi=function(){var a=F.body,b=F.documentElement||a&&a.parentElement,c,d;if(F.compatMode&&"BackCompat"!==F.compatMode)c=b?b.clientHeight:0,d=b?b.clientWidth:0;else{var e=function(g,h){return g&&h?Math.min(g,h):Math.max(g,h)};I("GTM",7);c=e(b?b.clientHeight:0,a?a.clientHeight:0);d=e(b?b.clientWidth:0,a?a.clientWidth:0)}return{width:d,height:c}},xi=function(a){var b=wi(),c=b.height,d=b.width,e=a.getBoundingClientRect(),g=e.bottom-e.top,h=e.right-e.left;return g&&h?(1-Math.min((Math.max(0-e.left,0)+
Math.max(e.right-d,0))/h,1))*(1-Math.min((Math.max(0-e.top,0)+Math.max(e.bottom-c,0))/g,1)):0},yi=function(a){if(F.hidden)return!0;var b=a.getBoundingClientRect();if(b.top==b.bottom||b.left==b.right||!D.getComputedStyle)return!0;var c=D.getComputedStyle(a,null);if("hidden"===c.visibility)return!0;for(var d=a,e=c;d;){if("none"===e.display)return!0;var g=e.opacity,h=e.filter;if(h){var k=h.indexOf("opacity(");0<=k&&(h=h.substring(k+8,h.indexOf(")",k)),"%"==h.charAt(h.length-1)&&(h=h.substring(0,h.length-
1)),g=Math.min(h,g))}if(void 0!==g&&0>=g)return!0;(d=d.parentElement)&&(e=D.getComputedStyle(d,null))}return!1};
var Gi=function(a,b,c){function d(){var h=a();g+=e?(Fa()-e)*h.playbackRate/1E3:0;e=Fa()}var e=0,g=0;return{Nb:function(h,k,l){var m=a(),n=m.Ud,q=void 0!==l?Math.round(l):void 0!==k?Math.round(m.Ud*k):Math.round(m.Yf),u=void 0!==k?Math.round(100*k):0>=n?0:Math.round(q/n*100),p=F.hidden?!1:.5<=xi(c);d();var r=Hg(c,"gtm.video",[b]);r["gtm.videoProvider"]="youtube";r["gtm.videoStatus"]=h;r["gtm.videoUrl"]=m.url;r["gtm.videoTitle"]=m.title;r["gtm.videoDuration"]=Math.round(n);r["gtm.videoCurrentTime"]=
Math.round(q);r["gtm.videoElapsedTime"]=Math.round(g);r["gtm.videoPercent"]=u;r["gtm.videoVisible"]=p;wg(r)},bh:function(){e=Fa()},xc:function(){d()}}};var Hi=D.clearTimeout,Ii=D.setTimeout,T=function(a,b,c){if(Ld()){b&&G(b)}else return cc(a,b,c)},Ji=function(){return D.location.href},Ki=function(a){return Pe(Qe(a),"fragment")},Li=function(a){return Oe(Qe(a))},V=function(a,b){return Cd(a,b||2)},Mi=function(a,b,c){b&&(a.eventCallback=b,c&&(a.eventTimeout=c));return wg(a)},Ni=function(a,b){D[a]=b},W=function(a,b,c){b&&(void 0===D[a]||c&&!D[a])&&(D[a]=b);return D[a]},
Oi=function(a,b,c){return If(a,b,void 0===c?!0:!!c)},Pi=function(a,b){if(Ld()){b&&G(b)}else ec(a,b)},Qi=function(a){return!!Lg(a,"init",!1)},Ri=function(a){Jg(a,"init",!0)},Si=function(a,b){var c=(void 0===b?0:b)?"www.googletagmanager.com/gtag/js":Sc;c+="?id="+encodeURIComponent(a)+"&l=dataLayer";T(P("https://","http://",c))},Ti=function(a,b){var c=a[b];return c};
var Ui=Eg.Hg;var Vi;var rj=new xa;function sj(a,b){function c(h){var k=Qe(h),l=Pe(k,"protocol"),m=Pe(k,"host",!0),n=Pe(k,"port"),q=Pe(k,"path").toLowerCase().replace(/\/$/,"");if(void 0===l||"http"==l&&"80"==n||"https"==l&&"443"==n)l="web",n="default";return[l,m,n,q]}for(var d=c(String(a)),e=c(String(b)),g=0;g<d.length;g++)if(d[g]!==e[g])return!1;return!0}
function tj(a){return uj(a)?1:0}
function uj(a){var b=a.arg0,c=a.arg1;if(a.any_of&&ua(c)){for(var d=0;d<c.length;d++)if(tj({"function":a["function"],arg0:b,arg1:c[d]}))return!0;return!1}switch(a["function"]){case "_cn":return 0<=String(b).indexOf(String(c));case "_css":var e;a:{if(b){var g=["matches","webkitMatchesSelector","mozMatchesSelector","msMatchesSelector","oMatchesSelector"];try{for(var h=0;h<g.length;h++)if(b[g[h]]){e=b[g[h]](c);break a}}catch(v){}}e=!1}return e;case "_ew":var k,l;k=String(b);l=String(c);var m=k.length-
l.length;return 0<=m&&k.indexOf(l,m)==m;case "_eq":return String(b)==String(c);case "_ge":return Number(b)>=Number(c);case "_gt":return Number(b)>Number(c);case "_lc":var n;n=String(b).split(",");return 0<=t(n,String(c));case "_le":return Number(b)<=Number(c);case "_lt":return Number(b)<Number(c);case "_re":var q;var u=a.ignore_case?"i":void 0;try{var p=String(c)+u,r=rj.get(p);r||(r=new RegExp(c,u),rj.set(p,r));q=r.test(b)}catch(v){q=!1}return q;case "_sw":return 0==String(b).indexOf(String(c));case "_um":return sj(b,
c)}return!1};var vj=function(a,b){var c=function(){};c.prototype=a.prototype;var d=new c;a.apply(d,Array.prototype.slice.call(arguments,1));return d};var wj={},xj=encodeURI,X=encodeURIComponent,yj=fc;var zj=function(a,b){if(!a)return!1;var c=Pe(Qe(a),"host");if(!c)return!1;for(var d=0;b&&d<b.length;d++){var e=b[d]&&b[d].toLowerCase();if(e){var g=c.length-e.length;0<g&&"."!=e.charAt(0)&&(g--,e="."+e);if(0<=g&&c.indexOf(e,g)==g)return!0}}return!1};
var Aj=function(a,b,c){for(var d={},e=!1,g=0;a&&g<a.length;g++)a[g]&&a[g].hasOwnProperty(b)&&a[g].hasOwnProperty(c)&&(d[a[g][b]]=a[g][c],e=!0);return e?d:null};wj.Eg=function(){var a=!1;return a};var Kk=function(){var a=D.gaGlobal=D.gaGlobal||{};a.hid=a.hid||wa();return a.hid};var Vk=window,Wk=document,Xk=function(a){var b=Vk._gaUserPrefs;if(b&&b.ioo&&b.ioo()||a&&!0===Vk["ga-disable-"+a])return!0;try{var c=Vk.external;if(c&&c._gaUserPrefs&&"oo"==c._gaUserPrefs)return!0}catch(g){}for(var d=If("AMP_TOKEN",Wk.cookie,!0),e=0;e<d.length;e++)if("$OPT_OUT"==d[e])return!0;return Wk.getElementById("__gaOptOutExtension")?!0:!1};function $k(a){return"_"===a.charAt(0)}var al=function(a){za(a,function(c){$k(c)&&delete a[c]});var b=a[H.V]||{};za(b,function(c){$k(c)&&delete b[c]})};var el=function(a,b,c){Bf(b,c,a)},fl=function(a,b,c){Bf(b,c,a,!0)},hl=function(a,b){};
function gl(a,b){}var Y={a:{}};


Y.a.jsm=["customScripts"],function(){(function(a){Y.__jsm=a;Y.__jsm.b="jsm";Y.__jsm.g=!0;Y.__jsm.priorityOverride=0})(function(a){if(void 0!==a.vtp_javascript){var b=a.vtp_javascript;try{var c=W("google_tag_manager");return c&&c.e&&c.e(b)}catch(d){}}})}();
Y.a.sp=["google"],function(){(function(a){Y.__sp=a;Y.__sp.b="sp";Y.__sp.g=!0;Y.__sp.priorityOverride=0})(function(a){var b=-1==navigator.userAgent.toLowerCase().indexOf("firefox")?"//www.googleadservices.com/pagead/conversion_async.js":"https://www.google.com/pagead/conversion_async.js",c=a.vtp_gtmOnFailure;ye();T(b,function(){var d=W("google_trackConversion");if(qa(d)){var e={};"DATA_LAYER"==a.vtp_customParamsFormat?e=a.vtp_dataLayerVariable:"USER_SPECIFIED"==a.vtp_customParamsFormat&&(e=Aj(a.vtp_customParams,
"key","value"));var g={};a.vtp_enableDynamicRemarketing&&(a.vtp_eventName&&(e.event=a.vtp_eventName),a.vtp_eventValue&&(g.value=a.vtp_eventValue),a.vtp_eventItems&&(g.items=a.vtp_eventItems));var h={google_conversion_id:a.vtp_conversionId,google_conversion_label:a.vtp_conversionLabel,google_custom_params:e,google_gtag_event_data:g,google_remarketing_only:!0,onload_callback:a.vtp_gtmOnSuccess,google_gtm:Yh()};a.vtp_rdp&&(h.google_restricted_data_processing=!0);d(h)||c()}else c()},c)})}();

Y.a.e=["google"],function(){(function(a){Y.__e=a;Y.__e.b="e";Y.__e.g=!0;Y.__e.priorityOverride=0})(function(a){return String(Kd(a.vtp_gtmEventId,"event"))})}();
Y.a.f=["google"],function(){(function(a){Y.__f=a;Y.__f.b="f";Y.__f.g=!0;Y.__f.priorityOverride=0})(function(a){var b=V("gtm.referrer",1)||F.referrer;return b?a.vtp_component&&"URL"!=a.vtp_component?Pe(Qe(String(b)),a.vtp_component,a.vtp_stripWww,a.vtp_defaultPages,a.vtp_queryKey):Li(String(b)):String(b)})}();
Y.a.cl=["google"],function(){function a(b){var c=b.target;if(c){var d=Hg(c,"gtm.click");Mi(d)}}(function(b){Y.__cl=b;Y.__cl.b="cl";Y.__cl.g=!0;Y.__cl.priorityOverride=0})(function(b){if(!Qi("cl")){var c=W("document");gc(c,"click",a,!0);Ri("cl")}G(b.vtp_gtmOnSuccess)})}();

Y.a.u=["google"],function(){var a=function(b){return{toString:function(){return b}}};(function(b){Y.__u=b;Y.__u.b="u";Y.__u.g=!0;Y.__u.priorityOverride=0})(function(b){var c;c=(c=b.vtp_customUrlSource?b.vtp_customUrlSource:V("gtm.url",1))||Ji();var d=b[a("vtp_component")];if(!d||"URL"==d)return Li(String(c));var e=Qe(String(c)),g;if("QUERY"===d)a:{var h=b[a("vtp_multiQueryKeys").toString()],k=b[a("vtp_queryKey").toString()]||"",l=b[a("vtp_ignoreEmptyQueryParam").toString()],m;m=h?ua(k)?k:String(k).replace(/\s+/g,
"").split(","):[String(k)];for(var n=0;n<m.length;n++){var q=Pe(e,"QUERY",void 0,void 0,m[n]);if(void 0!=q&&(!l||""!==q)){g=q;break a}}g=void 0}else g=Pe(e,d,"HOST"==d?b[a("vtp_stripWww")]:void 0,"PATH"==d?b[a("vtp_defaultPages")]:void 0,void 0);return g})}();
Y.a.v=["google"],function(){(function(a){Y.__v=a;Y.__v.b="v";Y.__v.g=!0;Y.__v.priorityOverride=0})(function(a){var b=a.vtp_name;if(!b||!b.replace)return!1;var c=V(b.replace(/\\\./g,"."),a.vtp_dataLayerVersion||1);return void 0!==c?c:a.vtp_defaultValue})}();
Y.a.ua=["google"],function(){var a,b={},c=function(d){var e={},g={},h={},k={},l={},m=void 0;if(d.vtp_gaSettings){var n=d.vtp_gaSettings;C(Aj(n.vtp_fieldsToSet,"fieldName","value"),g);C(Aj(n.vtp_contentGroup,"index","group"),h);C(Aj(n.vtp_dimension,"index","dimension"),k);C(Aj(n.vtp_metric,"index","metric"),l);d.vtp_gaSettings=null;n.vtp_fieldsToSet=void 0;n.vtp_contentGroup=void 0;n.vtp_dimension=void 0;n.vtp_metric=void 0;var q=C(n);d=C(d,q)}C(Aj(d.vtp_fieldsToSet,"fieldName","value"),g);C(Aj(d.vtp_contentGroup,
"index","group"),h);C(Aj(d.vtp_dimension,"index","dimension"),k);C(Aj(d.vtp_metric,"index","metric"),l);var u=Fe(d.vtp_functionName);if(qa(u)){var p="",r="";d.vtp_setTrackerName&&"string"==typeof d.vtp_trackerName?""!==d.vtp_trackerName&&(r=d.vtp_trackerName,p=r+"."):(r="gtm"+Zc(),p=r+".");var v={name:!0,clientId:!0,sampleRate:!0,siteSpeedSampleRate:!0,alwaysSendReferrer:!0,allowAnchor:!0,allowLinker:!0,cookieName:!0,cookieDomain:!0,cookieExpires:!0,cookiePath:!0,cookieUpdate:!0,legacyCookieDomain:!0,
legacyHistoryImport:!0,storage:!0,useAmpClientId:!0,storeGac:!0},w={allowAnchor:!0,allowLinker:!0,alwaysSendReferrer:!0,anonymizeIp:!0,cookieUpdate:!0,exFatal:!0,forceSSL:!0,javaEnabled:!0,legacyHistoryImport:!0,nonInteraction:!0,useAmpClientId:!0,useBeacon:!0,storeGac:!0,allowAdFeatures:!0},y=function(O){var K=[].slice.call(arguments,0);K[0]=p+K[0];u.apply(window,K)},x=function(O,K){return void 0===K?K:O(K)},z=function(O,K){if(K)for(var sa in K)K.hasOwnProperty(sa)&&y("set",O+sa,K[sa])},B=function(){},A=function(O,K,sa){var Eb=0;if(O)for(var Ba in O)if(O.hasOwnProperty(Ba)&&(sa&&v[Ba]||!sa&&void 0===v[Ba])){var Za=w[Ba]?Ca(O[Ba]):O[Ba];"anonymizeIp"!=Ba||Za||(Za=void 0);K[Ba]=Za;Eb++}return Eb},E={name:r};A(g,E,!0);u("create",d.vtp_trackingId||e.trackingId,E);y("set","&gtm",Yh(!0));d.vtp_enableRecaptcha&&y("require","recaptcha","recaptcha.js");(function(O,K){void 0!==d[K]&&y("set",O,d[K])})("nonInteraction","vtp_nonInteraction");z("contentGroup",h);z("dimension",k);z("metric",l);var J={};A(g,J,!1)&&y("set",J);var M;
d.vtp_enableLinkId&&y("require","linkid","linkid.js");y("set","hitCallback",function(){var O=g&&g.hitCallback;qa(O)&&O();d.vtp_gtmOnSuccess()});if("TRACK_EVENT"==d.vtp_trackType){d.vtp_enableEcommerce&&(y("require","ec","ec.js"),B());var R={hitType:"event",eventCategory:String(d.vtp_eventCategory||e.category),eventAction:String(d.vtp_eventAction||e.action),eventLabel:x(String,d.vtp_eventLabel||e.label),eventValue:x(Aa,d.vtp_eventValue||
e.value)};A(M,R,!1);y("send",R);}else if("TRACK_SOCIAL"==d.vtp_trackType){}else if("TRACK_TRANSACTION"==d.vtp_trackType){
y("require","ecommerce","//www.google-analytics.com/plugins/ua/ecommerce.js");var Q=function(O){return V("transaction"+O,1)},Z=Q("Id");y("ecommerce:addTransaction",{id:Z,affiliation:Q("Affiliation"),revenue:Q("Total"),shipping:Q("Shipping"),tax:Q("Tax")});for(var ea=Q("Products")||[],N=0;N<ea.length;N++){var L=ea[N];y("ecommerce:addItem",{id:Z,sku:L.sku,name:L.name,category:L.category,price:L.price,quantity:L.quantity})}y("ecommerce:send");}else if("TRACK_TIMING"==
d.vtp_trackType){}else if("DECORATE_LINK"==d.vtp_trackType){}else if("DECORATE_FORM"==d.vtp_trackType){}else if("TRACK_DATA"==d.vtp_trackType){}else{d.vtp_enableEcommerce&&(y("require","ec","ec.js"),B());if(d.vtp_doubleClick||"DISPLAY_FEATURES"==d.vtp_advertisingFeaturesType){var na=
"_dc_gtm_"+String(d.vtp_trackingId).replace(/[^A-Za-z0-9-]/g,"");y("require","displayfeatures",void 0,{cookieName:na})}if("DISPLAY_FEATURES_WITH_REMARKETING_LISTS"==d.vtp_advertisingFeaturesType){var ja="_dc_gtm_"+String(d.vtp_trackingId).replace(/[^A-Za-z0-9-]/g,"");y("require","adfeatures",{cookieName:ja})}M?y("send","pageview",M):y("send","pageview");}if(!a){var ta=d.vtp_useDebugVersion?"u/analytics_debug.js":"analytics.js";d.vtp_useInternalVersion&&!d.vtp_useDebugVersion&&(ta="internal/"+ta);a=!0;var ab=P("https:","http:","//www.google-analytics.com/"+ta,g&&g.forceSSL);T(ab,function(){var O=De();O&&O.loaded||d.vtp_gtmOnFailure();},d.vtp_gtmOnFailure)}}else G(d.vtp_gtmOnFailure)};Y.__ua=c;Y.__ua.b="ua";Y.__ua.g=!0;Y.__ua.priorityOverride=0}();



Y.a.ytl=["google"],function(){function a(){var w=Math.round(1E9*Math.random())+"";return F.getElementById(w)?a():w}function b(w,y){if(!w)return!1;for(var x=0;x<u.length;x++)if(0<=w.indexOf("//"+u[x]+"/"+y))return!0;return!1}function c(w){var y=-1!==w.indexOf("?")?"&":"?";if(-1<w.indexOf("origin="))return w+y+"enablejsapi=1";if(!r){var x=W("document");r=x.location.protocol+"//"+x.location.hostname;x.location.port&&(r+=":"+x.location.port)}return w+y+"enablejsapi=1&origin="+encodeURIComponent(r)}function d(w,
y){var x=w.getAttribute("src");if(b(x,"embed/")){if(0<x.indexOf("enablejsapi=1"))return!0;if(y)return w.setAttribute("src",c(x)),!0}return!1}function e(w,y){if(!w.getAttribute("data-gtm-yt-inspected-"+y.dd)&&(w.setAttribute("data-gtm-yt-inspected-"+y.dd,"true"),d(w,y.Yd))){w.id||(w.id=a());var x=W("YT"),z=x.get(w.id);z||(z=new x.Player(w.id));var B=h(z,y),A={},E;for(E in B)A.Pa=E,B.hasOwnProperty(A.Pa)&&z.addEventListener(A.Pa,function(J){return function(M){return B[J.Pa](M.data)}}(A)),A={Pa:A.Pa}}}
function g(w){G(function(){function y(){for(var z=x.getElementsByTagName("iframe"),B=z.length,A=0;A<B;A++)e(z[A],w)}var x=W("document");y();li(y)})}function h(w,y){var x,z;function B(){ea=Gi(function(){return{url:S,title:U,Ud:L,Yf:w.getCurrentTime(),playbackRate:na}},y.dd,w.getIframe());L=0;U=S="";na=1;return A}function A(O){switch(O){case p.PLAYING:L=Math.round(w.getDuration());S=w.getVideoUrl();if(w.getVideoData){var K=w.getVideoData();U=K?K.title:""}na=w.getPlaybackRate();y.Rf?ea.Nb("start"):ea.xc();
N=m(y.Rg,y.Qg,w.getDuration());return E(O);default:return A}}function E(){ja=w.getCurrentTime();ta=(new Date).getTime();ea.bh();Z();return J}function J(O){var K;switch(O){case p.ENDED:return R(O);case p.PAUSED:K="pause";case p.BUFFERING:var sa=w.getCurrentTime()-ja;K=1<Math.abs(((new Date).getTime()-ta)/1E3*na-sa)?"seek":K||"buffering";w.getCurrentTime()&&(y.Qf?ea.Nb(K):ea.xc());Q();return M;case p.UNSTARTED:return B(O);default:return J}}function M(O){switch(O){case p.ENDED:return R(O);case p.PLAYING:return E(O);
case p.UNSTARTED:return B(O);default:return M}}function R(){for(;z;){var O=x;Hi(z);O()}y.Pf&&ea.Nb("complete",1);return B(p.UNSTARTED)}function ca(){}function Q(){z&&(Hi(z),z=0,x=ca)}function Z(){if(N.length&&0!==na){var O=-1,K;do{K=N[0];if(K.qa>w.getDuration())return;O=(K.qa-w.getCurrentTime())/na;if(0>O&&(N.shift(),0===N.length))return}while(0>O);x=function(){z=0;x=ca;0<N.length&&N[0].qa===K.qa&&(N.shift(),ea.Nb("progress",K.ne,K.ve));Z()};z=Ii(x,1E3*O)}}var ea,N=[],L,S,U,na,ja,ta,ab=B(p.UNSTARTED);
z=0;x=ca;return{onStateChange:function(O){ab=ab(O)},onPlaybackRateChange:function(O){ja=w.getCurrentTime();ta=(new Date).getTime();ea.xc();na=O;Q();Z()}}}function k(w){for(var y=w.split(","),x=y.length,z=[],B=0;B<x;B++){var A=parseInt(y[B],10);isNaN(A)||100<A||0>A||z.push(A/100)}z.sort(function(E,J){return E-J});return z}function l(w){for(var y=w.split(","),x=y.length,z=[],B=0;B<x;B++){var A=parseInt(y[B],10);isNaN(A)||0>A||z.push(A)}z.sort(function(E,J){return E-J});return z}function m(w,y,x){var z=
w.map(function(E){return{qa:E,ve:E,ne:void 0}});if(!y.length)return z;var B=y.map(function(E){return{qa:E*x,ve:void 0,ne:E}});if(!z.length)return B;var A=z.concat(B);A.sort(function(E,J){return E.qa-J.qa});return A}function n(w){w.vtp_triggerStartOption?q(w):pe(function(){q(w)})}function q(w){var y=!!w.vtp_captureStart,x=!!w.vtp_captureComplete,z=!!w.vtp_capturePause,B=k(w.vtp_progressThresholdsPercent+""),A=l(w.vtp_progressThresholdsTimeInSeconds+""),E=!!w.vtp_fixMissingApi;if(y||x||z||B.length||
A.length){var J={Rf:y,Pf:x,Qf:z,Qg:B,Rg:A,Yd:E,dd:void 0===w.vtp_uniqueTriggerId?"":w.vtp_uniqueTriggerId},M=W("YT"),R=function(){g(J)};G(w.vtp_gtmOnSuccess);if(M)M.ready&&M.ready(R);else{var ca=W("onYouTubeIframeAPIReady");Ni("onYouTubeIframeAPIReady",function(){ca&&ca();R()});G(function(){for(var Q=W("document"),Z=Q.getElementsByTagName("script"),ea=Z.length,N=0;N<ea;N++){var L=Z[N].getAttribute("src");if(b(L,"iframe_api")||b(L,"player_api"))return}for(var S=Q.getElementsByTagName("iframe"),U=S.length,
na=0;na<U;na++)if(!v&&d(S[na],J.Yd)){T("https://www.youtube.com/iframe_api");v=!0;break}})}}else G(w.vtp_gtmOnSuccess)}var u=["www.youtube.com","www.youtube-nocookie.com"],p={UNSTARTED:-1,ENDED:0,PLAYING:1,PAUSED:2,BUFFERING:3,CUED:5},r,v=!1;Y.__ytl=n;Y.__ytl.b="ytl";Y.__ytl.g=!0;Y.__ytl.priorityOverride=0}();
Y.a.cid=["google"],function(){(function(a){Y.__cid=a;Y.__cid.b="cid";Y.__cid.g=!0;Y.__cid.priorityOverride=0})(function(){return Nc.m})}();

Y.a.gclidw=["google"],function(){var a=["aw","dc","gf","ha","gp"];(function(b){Y.__gclidw=b;Y.__gclidw.b="gclidw";Y.__gclidw.g=!0;Y.__gclidw.priorityOverride=100})(function(b){G(b.vtp_gtmOnSuccess);var c,d,e;b.vtp_enableCookieOverrides&&(e=b.vtp_cookiePrefix,c=b.vtp_path,d=b.vtp_domain);var g=null;b.vtp_enableCookieUpdateFeature&&(g=!0,void 0!==b.vtp_cookieUpdate&&(g=!!b.vtp_cookieUpdate));var h=e,k=c,l=d;if(b.vtp_enableCrossDomainFeature&&(!b.vtp_enableCrossDomain||!1!==b.vtp_acceptIncoming)&&(b.vtp_enableCrossDomain||
xh())){Lh(a,h,k,l,void 0);}var m={prefix:e,path:c,domain:d,Ja:void 0};Ih(m);Ph(["aw","dc"],m);Uh(g,e,c,d);var n=e;if(b.vtp_enableCrossDomainFeature&&b.vtp_enableCrossDomain&&b.vtp_linkerDomains){var q=b.vtp_linkerDomains.toString().replace(/\s+/g,"").split(","),u=b.vtp_urlPosition,p=!!b.vtp_formDecoration;Nh(a,q,u,p,n);}})}();


Y.a.aev=["google"],function(){function a(p,r){var v=Kd(p,"gtm");if(v)return v[r]}function b(p,r,v,w){w||(w="element");var y=p+"."+r,x;if(n.hasOwnProperty(y))x=n[y];else{var z=a(p,w);if(z&&(x=v(z),n[y]=x,q.push(y),35<q.length)){var B=q.shift();delete n[B]}}return x}function c(p,r,v){var w=a(p,u[r]);return void 0!==w?w:v}function d(p,r){if(!p)return!1;var v=e(Ji());ua(r)||(r=String(r||"").replace(/\s+/g,"").split(","));for(var w=[v],y=0;y<r.length;y++)if(r[y]instanceof RegExp){if(r[y].test(p))return!1}else{var x=
r[y];if(0!=x.length){if(0<=e(p).indexOf(x))return!1;w.push(e(x))}}return!zj(p,w)}function e(p){m.test(p)||(p="http://"+p);return Pe(Qe(p),"HOST",!0)}function g(p,r,v){switch(p){case "SUBMIT_TEXT":return b(r,"FORM."+p,h,"formSubmitElement")||v;case "LENGTH":var w=b(r,"FORM."+p,k);return void 0===w?v:w;case "INTERACTED_FIELD_ID":return l(r,"id",v);case "INTERACTED_FIELD_NAME":return l(r,"name",v);case "INTERACTED_FIELD_TYPE":return l(r,"type",v);case "INTERACTED_FIELD_POSITION":var y=a(r,"interactedFormFieldPosition");
return void 0===y?v:y;case "INTERACT_SEQUENCE_NUMBER":var x=a(r,"interactSequenceNumber");return void 0===x?v:x;default:return v}}function h(p){switch(p.tagName.toLowerCase()){case "input":return ic(p,"value");case "button":return jc(p);default:return null}}function k(p){if("form"===p.tagName.toLowerCase()&&p.elements){for(var r=0,v=0;v<p.elements.length;v++)ei(p.elements[v])&&r++;return r}}function l(p,r,v){var w=a(p,"interactedFormField");return w&&ic(w,r)||v}var m=/^https?:\/\//i,n={},q=[],u={ATTRIBUTE:"elementAttribute",
CLASSES:"elementClasses",ELEMENT:"element",ID:"elementId",HISTORY_CHANGE_SOURCE:"historyChangeSource",HISTORY_NEW_STATE:"newHistoryState",HISTORY_NEW_URL_FRAGMENT:"newUrlFragment",HISTORY_OLD_STATE:"oldHistoryState",HISTORY_OLD_URL_FRAGMENT:"oldUrlFragment",TARGET:"elementTarget"};(function(p){Y.__aev=p;Y.__aev.b="aev";Y.__aev.g=!0;Y.__aev.priorityOverride=0})(function(p){var r=p.vtp_gtmEventId,v=p.vtp_defaultValue,w=p.vtp_varType;switch(w){case "TAG_NAME":var y=a(r,"element");return y&&y.tagName||
v;case "TEXT":return b(r,w,jc)||v;case "URL":var x;a:{var z=String(a(r,"elementUrl")||v||""),B=Qe(z),A=String(p.vtp_component||"URL");switch(A){case "URL":x=z;break a;case "IS_OUTBOUND":x=d(z,p.vtp_affiliatedDomains);break a;default:x=Pe(B,A,p.vtp_stripWww,p.vtp_defaultPages,p.vtp_queryKey)}}return x;case "ATTRIBUTE":var E;if(void 0===p.vtp_attribute)E=c(r,w,v);else{var J=p.vtp_attribute,M=a(r,"element");E=M&&ic(M,J)||v||""}return E;case "MD":var R=p.vtp_mdValue,ca=b(r,"MD",si);return R&&ca?vi(ca,
R)||v:ca||v;case "FORM":return g(String(p.vtp_component||"SUBMIT_TEXT"),r,v);default:return c(r,w,v)}})}();
Y.a.gas=["google"],function(){(function(a){Y.__gas=a;Y.__gas.b="gas";Y.__gas.g=!0;Y.__gas.priorityOverride=0})(function(a){var b=C(a),c=b;c[Gb.ma]=null;c[Gb.ff]=null;var d=b=c;d.vtp_fieldsToSet=d.vtp_fieldsToSet||[];var e=d.vtp_cookieDomain;void 0!==e&&(d.vtp_fieldsToSet.push({fieldName:"cookieDomain",value:e}),delete d.vtp_cookieDomain);return b})}();

Y.a.awct=["google"],function(){var a=!1,b=[],c=function(k){var l=W("google_trackConversion"),m=k.gtm_onFailure;"function"==typeof l?l(k)||m():m()},d=function(){for(;0<b.length;)c(b.shift())},e=function(){return function(){d();a=!1}},g=function(){return function(){d();b={push:c};}},h=function(k){ye();var l={google_basket_transaction_type:"purchase",google_conversion_domain:"",google_conversion_id:k.vtp_conversionId,google_conversion_label:k.vtp_conversionLabel,
google_conversion_value:k.vtp_conversionValue||0,google_remarketing_only:!1,onload_callback:k.vtp_gtmOnSuccess,gtm_onFailure:k.vtp_gtmOnFailure,google_gtm:Yh()};k.vtp_rdp&&(l.google_restricted_data_processing=!0);var m=function(v){return function(w,y,x){var z="DATA_LAYER"==v?V(x):k[y];z&&(l[w]=z)}},n=m("JSON");n("google_conversion_currency","vtp_currencyCode");n("google_conversion_order_id","vtp_orderId");k.vtp_enableProductReporting&&(n=m(k.vtp_productReportingDataSource),n("google_conversion_merchant_id",
"vtp_awMerchantId","aw_merchant_id"),n("google_basket_feed_country","vtp_awFeedCountry","aw_feed_country"),n("google_basket_feed_language","vtp_awFeedLanguage","aw_feed_language"),n("google_basket_discount","vtp_discount","discount"),n("google_conversion_items","vtp_items","items"),l.google_conversion_items=l.google_conversion_items.map(function(v){return{value:v.price,quantity:v.quantity,item_id:v.id}}));var q=function(v,w){(l.google_additional_conversion_params=l.google_additional_conversion_params||
{})[v]=w},u=function(v){return function(w,y,x,z){var B="DATA_LAYER"==v?V(x):k[y];z(B)&&q(w,B)}},p=-1==navigator.userAgent.toLowerCase().indexOf("firefox")?"//www.googleadservices.com/pagead/conversion_async.js":"https://www.google.com/pagead/conversion_async.js";k.vtp_enableNewCustomerReporting&&(n=u(k.vtp_newCustomerReportingDataSource),n("vdnc","vtp_awNewCustomer","new_customer",function(v){return void 0!=v&&""!==v}),n("vdltv","vtp_awCustomerLTV","customer_lifetime_value",function(v){return void 0!=
v&&""!==v}));!k.hasOwnProperty("vtp_enableConversionLinker")||k.vtp_enableConversionLinker?(k.vtp_conversionCookiePrefix&&(l.google_gcl_cookie_prefix=k.vtp_conversionCookiePrefix),l.google_read_gcl_cookie_opt_out=!1):l.google_read_gcl_cookie_opt_out=!0;var r=!0;r&&b.push(l);a||(a=!0,T(p,g(),e(p)))};Y.__awct=h;Y.__awct.b="awct";Y.__awct.g=!0;Y.__awct.priorityOverride=0}();
Y.a.fsl=[],function(){function a(){var e=W("document"),g=c(),h=HTMLFormElement.prototype.submit;gc(e,"click",function(k){var l=k.target;if(l&&(l=lc(l,["button","input"],100))&&("submit"==l.type||"image"==l.type)&&l.name&&ic(l,"value")){var m;(m=l.form?l.form.tagName?l.form:F.getElementById(l.form):lc(l,["form"],100))&&g.store(m,l)}},!1);gc(e,"submit",function(k){var l=k.target;if(!l)return k.returnValue;var m=k.defaultPrevented||!1===k.returnValue,n=b(l)&&!m,q=g.get(l),u=!0;if(d(l,function(){if(u){var p;
q&&(p=e.createElement("input"),p.type="hidden",p.name=q.name,p.value=q.value,l.appendChild(p));h.call(l);p&&l.removeChild(p)}},m,n,q))u=!1;else return m||(k.preventDefault&&k.preventDefault(),k.returnValue=!1),!1;return k.returnValue},!1);HTMLFormElement.prototype.submit=function(){var k=this,l=b(k),m=!0;d(k,function(){m&&h.call(k)},!1,l)&&(h.call(k),m=!1)}}function b(e){var g=e.target;return g&&"_self"!==g&&"_parent"!==g&&"_top"!==g?!1:!0}function c(){var e=[],g=function(h){return va(e,function(k){return k.form===
h})};return{store:function(h,k){var l=g(h);l?l.button=k:e.push({form:h,button:k})},get:function(h){var k=g(h);return k?k.button:null}}}function d(e,g,h,k,l){var m=Lg("fsl",h?"nv.mwt":"mwt",0),n;n=h?Lg("fsl","nv.ids",[]):Lg("fsl","ids",[]);if(!n.length)return!0;var q=Hg(e,"gtm.formSubmit",n),u=e.action;u&&u.tagName&&(u=e.cloneNode(!1).action);q["gtm.elementUrl"]=u;l&&(q["gtm.formSubmitElement"]=l);if(k&&m){if(!Mi(q,xg(g),m))return!1}else Mi(q,function(){},m||2E3);return!0}(function(e){Y.__fsl=e;Y.__fsl.b=
"fsl";Y.__fsl.g=!0;Y.__fsl.priorityOverride=0})(function(e){var g=e.vtp_waitForTags,h=e.vtp_checkValidation,k=Number(e.vtp_waitForTagsTimeout);if(!k||0>=k)k=2E3;var l=e.vtp_uniqueTriggerId||"0";if(g){var m=function(q){return Math.max(k,q)};Kg("fsl","mwt",m,0);h||Kg("fsl","nv.mwt",m,0)}var n=function(q){q.push(l);return q};Kg("fsl","ids",n,[]);h||Kg("fsl","nv.ids",n,[]);Qi("fsl")||(a(),Ri("fsl"));G(e.vtp_gtmOnSuccess)})}();



Y.a.paused=[],function(){(function(a){Y.__paused=a;Y.__paused.b="paused";Y.__paused.g=!0;Y.__paused.priorityOverride=0})(function(a){G(a.vtp_gtmOnFailure)})}();Y.a.hid=["google"],function(){(function(a){Y.__hid=a;Y.__hid.b="hid";Y.__hid.g=!0;Y.__hid.priorityOverride=0})(function(){return Eg.zb})}();
Y.a.html=["customScripts"],function(){function a(d,e,g,h){return function(){try{if(0<e.length){var k=e.shift(),l=a(d,e,g,h);if("SCRIPT"==String(k.nodeName).toUpperCase()&&"text/gtmscript"==k.type){var m=F.createElement("script");m.async=!1;m.type="text/javascript";m.id=k.id;m.text=k.text||k.textContent||k.innerHTML||"";k.charset&&(m.charset=k.charset);var n=k.getAttribute("data-gtmsrc");n&&(m.src=n,bc(m,l));d.insertBefore(m,null);n||l()}else if(k.innerHTML&&0<=k.innerHTML.toLowerCase().indexOf("<script")){for(var q=
[];k.firstChild;)q.push(k.removeChild(k.firstChild));d.insertBefore(k,null);a(k,q,l,h)()}else d.insertBefore(k,null),l()}else g()}catch(u){G(h)}}}var c=function(d){if(F.body){var e=
d.vtp_gtmOnFailure,g=Ui(d.vtp_html,d.vtp_gtmOnSuccess,e),h=g.Hc,k=g.s;if(d.vtp_useIframe){}else d.vtp_supportDocumentWrite?b(h,k,e):a(F.body,kc(h),k,e)()}else Ii(function(){c(d)},
200)};Y.__html=c;Y.__html.b="html";Y.__html.g=!0;Y.__html.priorityOverride=0}();








var $l={};$l.macro=function(a){if(Eg.wc.hasOwnProperty(a))return Eg.wc[a]},$l.onHtmlSuccess=Eg.Td(!0),$l.onHtmlFailure=Eg.Td(!1);$l.dataLayer=Dd;$l.callback=function(a){Xc.hasOwnProperty(a)&&qa(Xc[a])&&Xc[a]();delete Xc[a]};function am(){Oc[Nc.m]=$l;Ia(Yc,Y.a);xb=xb||Eg;yb=ge}
function bm(){zh.gtm_3pds=!0;Oc=D.google_tag_manager=D.google_tag_manager||{};if(Oc[Nc.m]){var a=Oc.zones;a&&a.unregisterChild(Nc.m)}else{for(var b=data.resource||{},c=b.macros||[],d=0;d<c.length;d++)pb.push(c[d]);for(var e=b.tags||[],g=0;g<e.length;g++)sb.push(e[g]);for(var h=b.predicates||[],k=0;k<
h.length;k++)rb.push(h[k]);for(var l=b.rules||[],m=0;m<l.length;m++){for(var n=l[m],q={},u=0;u<n.length;u++)q[n[u][0]]=Array.prototype.slice.call(n[u],1);qb.push(q)}vb=Y;wb=tj;am();Dg();ke=!1;le=0;if("interactive"==F.readyState&&!F.createEventObject||"complete"==F.readyState)ne();else{gc(F,"DOMContentLoaded",ne);gc(F,"readystatechange",ne);if(F.createEventObject&&F.documentElement.doScroll){var p=!0;try{p=!D.frameElement}catch(y){}p&&oe()}gc(D,"load",ne)}qg=!1;"complete"===F.readyState?sg():gc(D,
"load",sg);a:{if(!sd)break a;D.setInterval(td,864E5);}
Uc=(new Date).getTime();
}}bm();

})()
